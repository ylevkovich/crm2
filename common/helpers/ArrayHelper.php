<?php
/**
 * Created by PhpStorm.
 * User: Yurii.l
 * Date: 19.07.17
 * Time: 21:36
 */

namespace common\helpers;


class ArrayHelper
{
    public static function arrayToDropDown($array)
    {
        $htmlOptions = '';

        if(!empty($array)){
            foreach ($array as $key => $value){
                $htmlOptions .= "<option value='" . $key . "'>$value</option>";
            }
        }

        return $htmlOptions;
    }
}