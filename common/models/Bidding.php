<?php

namespace common\models;

/**
 * This is the model class for table "bidding".
 *
 * @property string $id
 * @property string $name
 * @property integer $is_active
 * @property integer $type
 * @property string $city
 * @property string $date
 * @property string $time_start
 * @property string $time_stop
 * @property integer $percent
 * @property integer $summ
 * @property integer $guaranted_percent
 * @property integer $status
 * @property integer $pdf_reglament
 * @property integer $dogovor
 * @property string $dogovor_datetime_start
 * @property string $dogovor_datetime_stop
 */
class Bidding extends \yii\db\ActiveRecord
{
    const STATUS_ON_SALE = 1;
    const STATUS_DEFAULT = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_BUY = 4;
    const STATUS_ONLINE = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bidding';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'date', 'time_start', 'time_stop', 'dogovor_datetime_start', 'dogovor_datetime_stop'], 'required'],
            [['is_active', 'type', 'percent', 'summ', 'guaranted_percent', 'status', 'pdf_reglament', 'dogovor'], 'integer'],
            [['date', 'time_start', 'time_stop', 'dogovor_datetime_start', 'dogovor_datetime_stop'], 'safe'],
            [['id'], 'string', 'max' => 2],
            [['name', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'is_active' => 'Is Active',
            'type' => 'Type',
            'city' => 'City',
            'date' => 'Date',
            'time_start' => 'Time Start',
            'time_stop' => 'Time Stop',
            'percent' => 'Percent',
            'summ' => 'Summ',
            'guaranted_percent' => 'Guaranted Percent',
            'status' => 'Status',
            'pdf_reglament' => 'Pdf Reglament',
            'dogovor' => 'Dogovor',
            'dogovor_datetime_start' => 'Dogovor Datetime Start',
            'dogovor_datetime_stop' => 'Dogovor Datetime Stop',
        ];
    }
}
