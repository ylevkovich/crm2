<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $id_company
 * @property string $full_name
 * @property string $email
 * @property integer $status
 * @property integer $role
 * @property string $phone
 * @property integer $idrpo
 * @property integer $hidden_auction_access
 * @property string $legal_person
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $activate_code
 * @property string $position
 *
 * @property Company $idCompany
 * @property string $new_password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_ACCOUNTENT = 3;
    const ROLE_BUYER = 4;
    const ROLE_SELLER = 5;
    const ROLE_BROKER = 6;

    const STATUS_NOT_ACTIVATED = 0;
    const STATUS_ACTIVATED = 1;
    const STATUS_BLOKED = 10;

    const HIDDEN_AUCTION_DENIED = 0;
    const HIDDEN_AUCTION_ACCESS = 1;

    const ROLE_TRANSLATE = [
        self::ROLE_ADMIN => 'адміністратор',
        self::ROLE_MANAGER => 'менеджер',
        self::ROLE_ACCOUNTENT => 'бухгалтер',
        self::ROLE_BUYER => 'покупець',
        self::ROLE_SELLER => 'продавець',
        self::ROLE_BROKER => 'брокер',
    ];

    const STATUS_TRANSLATE = [
        self::STATUS_ACTIVATED => 'Активний',
        self::STATUS_NOT_ACTIVATED => 'Не активний',
        self::STATUS_BLOKED => 'Заблокований',
    ];

    public $new_password;
    


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'email', 'message' => 'Електронна адреса не правильного формату'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Дана електронна адреса вже використовується'],

            [['password_hash', 'new_password'], 'string', 'length' => [6, 255], 'message' => 'Пароль має складатися не манше як з 6 символів'],

            [['email', 'full_name', 'phone', 'status', 'legal_person', 'idrpo'], 'required', 'message' => 'Дане поле не може бути пустим'],
            [['status', 'role', 'hidden_auction_access', 'created_at', 'updated_at'], 'integer'],
//            ['idrpo', 'match', 'pattern' => '/^[0-9]{8,10}$/', 'message' => 'Дане поле має складатися з 8-10 символів'],
            ['idrpo','string', 'min' => 8,'max'=>10, 'tooLong' => 'Дане поле має складатися з 8-10 символів', 'tooShort' => 'Дане поле має складатися з 8-10 символів'],
            ['position', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->new_password != '') {
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->new_password);
            }
        }

        return true;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ІН',
            'id_company' => 'ІН Компанії',
            'full_name' => 'ПІБ',
            'email' => 'Електронна адреса',
            'status' => 'Статус',
            'role' => 'Роль',
            'phone' => 'Телефон',
            'position' => 'Посада',
            'idrpo' => 'ЄДРПОУ або ІПН',
            'hidden_auction_access' => 'Перегляд прихованих торгів',
            'legal_person' => 'Назва компанії або підприємця',
            'created_at' => 'Дата реєстрації',
            'updated_at' => 'Updated At',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'new_password' => 'Пароль',
            'search' => 'ПІБ або E-mail',
        ];
    }

    /**
     * Returns specified array of users
     * @param $role
     * @return array
     */
    public static function getUsersForDropDown($role, $id_company = null)
    {
        $users = User::find()
            ->where([
                'role' => $role,
                'status' => self::STATUS_ACTIVATED,
            ])
            ->andWhere(['or',
                ['id_company' => null],
                ['id_company' => $id_company]
            ])
            ->orderBy('full_name')
            ->all();

        $users = ArrayHelper::map($users, 'id', 'full_name');
//        $users[0] = '';
        ksort($users);

        return $users;
    }

    /**
     * Returns specified array of users
     *
     * @param $id_company
     * @return array|ActiveRecord[]
     */
    public static function getUsersCompanyForDropDown($id_company)
    {
        $users = User::find()
            ->where(['id_company' => $id_company])
            ->orderBy('full_name')
            ->all();

        $users = ArrayHelper::map($users, 'id', 'full_name');
        $users[0] = '';
        ksort($users);

        return $users;
    }

    public function isAdminPersonnel()
    {
        return in_array($this->role, [self::ROLE_ADMIN, self::ROLE_MANAGER, self::ROLE_ACCOUNTENT]) ? true : false;
    }

    public static function getAdminRolesArray()
    {
        return [
            self::ROLE_ADMIN => self::ROLE_TRANSLATE[self::ROLE_ADMIN],
            self::ROLE_MANAGER => self::ROLE_TRANSLATE[self::ROLE_MANAGER],
            self::ROLE_ACCOUNTENT => self::ROLE_TRANSLATE[self::ROLE_ACCOUNTENT],
        ];
    }

    public static function getUsersRolesArray()
    {
        return [
            self::ROLE_BUYER => self::ROLE_TRANSLATE[self::ROLE_BUYER],
            self::ROLE_SELLER => self::ROLE_TRANSLATE[self::ROLE_SELLER],
            self::ROLE_BROKER => self::ROLE_TRANSLATE[self::ROLE_BROKER],
        ];
    }

    public function getStatusName()
    {
        return $this::STATUS_TRANSLATE[$this->status];
    }

    public function getRoleName()
    {
        return $this::ROLE_TRANSLATE[$this->role];
    }


}
