<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "nomenclature".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_nomenclature_category
 * @property integer $breed
 * @property integer $sort
 * @property integer $diameter
 * @property string $state_standart
 * @property string $note
 *
 * @property NomenclatureCategory $idNomenclatureCategory
 */
class Nomenclature extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nomenclature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id_nomenclature_category', 'breed', 'diameter'], 'required', 'message' => 'Дане поле не може бути пустим'],
            [['id_nomenclature_category', 'sort', 'diameter'], 'integer', 'message' => 'Дане поле повинне бути цілим числом'],
            [['note', 'breed'], 'string'],
            [['name', 'state_standart'], 'string', 'max' => 255],
            [['id_nomenclature_category'], 'exist', 'skipOnError' => true, 'targetClass' => NomenclatureCategory::className(), 'targetAttribute' => ['id_nomenclature_category' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'id_nomenclature_category' => 'Список номенклатури',
            'breed' => 'Порода',
            'sort' => 'Сорт',
            'diameter' => 'Діаметр, см',
            'state_standart' => 'ГОСТ',
            'note' => 'Нотатки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNomenclatureCategory()
    {
        return $this->hasOne(NomenclatureCategory::className(), ['id' => 'id_nomenclature_category']);
    }

    public static function getChildCategoriesList()
    {
        return ArrayHelper::map(NomenclatureCategory::findAll(['id_parent' => !null]),'id','name');
    }
}
