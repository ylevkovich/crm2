<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $name
 * @property string $idrpo
 * @property integer $auction_type
 * @property integer $type
 * @property integer $accreditation
 * @property integer $document_package
 * @property string $email
 * @property string $account_number
 * @property string $bank_name
 * @property string $bank_mfo
 * @property integer $individual_tax_number
 * @property integer $certificate_of_vat
 * @property string $manager_position
 * @property string $manager_full_name
 * @property string $manager_full_name_genitive_case
 * @property integer $zip_code
 * @property string $country
 * @property string $region
 * @property string $area
 * @property string $city
 * @property string $street
 * @property string $phone
 * @property string $basic_contact
 * @property string $note
 * @property integer $role
 * @property integer $document_access
 * @property integer $localy_reworker
 * @property integer $documents_date_from
 * @property integer $documents_date_to
 * @property integer $documents_issued_by
 * @property integer $resident
 * @property integer $broker
 * @property integer $black_list
 *
 * @property User[] $users
 */
class Company extends \yii\db\ActiveRecord
{

    public $company_users;

    /**
     * Role must be like users role
     */
    const ROLE_BUYER = 4;
    const ROLE_SELLER = 5;
    const ROLE_BROKER = 6;

    const AUCTION_TYPE_UNTREATED_WOOD = 1;

    const TYPE_FORESTRY = 1;
    const TYPE_AGRO_FORESTRY = 2;
    const TYPE_MILITARY_FORESTRY = 3;

    const ROLE_TRANSLATE = [
        self::ROLE_BUYER => 'покупець',
        self::ROLE_SELLER => 'продавець',
        self::ROLE_BROKER => 'брокер',
    ];

    public static function auctionTypeTranslate()
    {

        return [
            self::AUCTION_TYPE_UNTREATED_WOOD => 'Необроблена деревина',
        ];
    }

    public static function typeTranslate()
    {

        return [
            self::TYPE_FORESTRY => 'Лісгосп',
            self::TYPE_AGRO_FORESTRY => 'Агролісгосп',
            self::TYPE_MILITARY_FORESTRY => 'Військовий лісгосп',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            // сереализация данных при смени типа компании
            if($this->role==self::ROLE_SELLER){
                $this->broker = 0;
                $this->document_access = null;
                $this->documents_date_from = null;
                $this->documents_date_to = null;
                $this->documents_issued_by = null;
                $this->localy_reworker = 0;
                $this->resident = 0;

            }
            if($this->role==self::ROLE_BUYER){
                $this->broker = 0;
                $this->type = 0;
            }
            if($this->role==self::ROLE_BROKER){
                $this->type = 0;
            }

            return true;
        }
        return false;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auction_type', 'type', 'accreditation', 'document_package', 'certificate_of_vat', 'zip_code', 'role', 'document_access', 'localy_reworker', /*'documents_date_from', 'documents_date_to',*/
                'resident', 'black_list', 'account_number',], 'integer', 'message' => 'Дане поле має бути числового формату'],
            [['name', 'documents_issued_by'], 'string', 'max' => 100],
            [['email', 'bank_name', 'bank_mfo', 'individual_tax_number', 'manager_position', 'manager_full_name', 'manager_full_name_genitive_case', 'country', 'region', 'area', 'city', 'street', 'phone', 'basic_contact', 'note'], 'string', 'max' => 255],
            ['email', 'email', 'message' => 'Електронна адреса не правильного формату'],
            ['name', 'required', 'message' => 'Дане поле не може бути пустим'],
            ['idrpo', 'string', 'min' => 8, 'max' => 10, 'tooLong' => 'Дане поле має складатися з 8-10 символів', 'tooShort' => 'Дане поле має складатися з 8-10 символів'],
            ['company_users', 'safe'],
            ['broker', 'boolean'],
//            ['idrpo', 'match', 'pattern' => '/^[0-9]{8,10}$/', 'message' => 'Дане поле має складатися з 8-10 символів'],
//            [['documents_date_from', 'documents_date_to'], 'match', 'pattern' => '/^[0-9]{2}/[0-9]{2}/[0-9]{4}$/', 'message' => 'Дане поле має бути певного формату - 07/04/2017'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'idrpo' => 'Код ЄДРПОУ',
            'auction_type' => 'Тип торгів',
            'type' => 'Тип',
            'accreditation' => 'Акредитовано',
            'document_package' => 'Пакет документів',
            'email' => 'Електронна пошта',
            'account_number' => 'Номер розрахункового рахунку',
            'bank_name' => 'Назва банку',
            'bank_mfo' => 'МФО банку',
            'individual_tax_number' => 'ІПН',
            'certificate_of_vat' => 'Свідоцтво платника ПДВ',
            'manager_position' => 'Посада керівника',
            'manager_full_name' => 'ПІБ керівника',
            'manager_full_name_genitive_case' => 'ПІБ керівника (родовий відмінок)',
            'zip_code' => 'Поштовий індекс',
            'country' => 'Країна',
            'region' => 'Область',
            'area' => 'Район',
            'city' => 'Населений пункт',
            'street' => 'Вулиця',
            'phone' => 'Контактний номер телефону',
            'basic_contact' => 'Підстава для договору',
            'note' => 'Нотатки',
            'role' => 'Роль компанії',
            'broker' => 'Брокер',

            'document_access' => 'Дозвільні документи',
            'localy_reworker' => 'Місцевий переробник',
            'documents_date_from' => 'З дати',
            'documents_date_to' => 'До дати',
            'documents_issued_by' => 'Виданий',
            'resident' => 'Резидент',
            'black_list' => 'Black List',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_company' => 'id']);
    }

    /**
     * Returns specified array of companies
     * @param $role
     * @return array
     */
    public static function getCompaniesForDropDown($role)
    {
        $companies = self::find()
            ->where(['role' => $role])
            ->orderBy('name')
            ->all();

        $companies = ArrayHelper::map($companies, 'id', 'name');
        $companies[''] = '';
        ksort($companies);

        return $companies;
    }

    /**
     * Return specefic array for dropdownlist to show selected items
     *
     * @param $id_company
     * @return array
     */
    public static function getSelectedUsersForDropDown($id_company)
    {
        $res = [];

        $users = User::find()
            ->select('id')
            ->where(['id_company' => $id_company])
            ->all();

        if (!empty($users)) {
            $users = ArrayHelper::toArray($users, 'id');

            foreach ($users as $user) {
                $res[$user['id']] = ['selected' => true];
            }
        }

        return $res;
    }

    /**
     * Looks for users and save company.id there
     *
     * @param Company $model
     * @return bool
     */
    public function linkUsersToCompany()
    {
        $users = $this->company_users;

        if (!empty($users) && $users[0] != 0) {
            foreach ($users as $user) {
                $userModel = User::findIdentity($user);
                $userModel->id_company = $this->id;

                if (!$userModel->save(false)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Clean field id_company from all users where id_company == $id_company
     *
     * @param $id_company
     * @return bool
     */
    public function cleanUsersFromCompanyId()
    {
        $users = User::find()
            ->where(['id_company' => $this->id])
            ->all();

        if (!empty($users)) {
            foreach ($users as $user) {
                $user->id_company = null;

                if (!$user->save(false)) {
                    return false;
                }
            }
        }

        return true;
    }


}
