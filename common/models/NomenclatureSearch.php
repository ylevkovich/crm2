<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Nomenclature;

/**
 * NomenclatureSearch represents the model behind the search form about `common\models\Nomenclature`.
 */
class NomenclatureSearch extends Nomenclature
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_nomenclature_category', 'breed', 'sort', 'diameter'], 'integer'],
            [['name', 'state_standart', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id_category)
    {
        $query = Nomenclature::find();

        $query->where(['id_nomenclature_category' => $id_category]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_nomenclature_category' => $this->id_nomenclature_category,
            'breed' => $this->breed,
            'sort' => $this->sort,
            'diameter' => $this->diameter,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'state_standart', $this->state_standart])
            ->andFilterWhere(['like', 'note', $this->note]);

        $query->orderBy('name');

        return $dataProvider;
    }
}
