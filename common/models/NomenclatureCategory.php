<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "nomenclature_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_parent
 *
 * @property Nomenclature[] $nomenclatures
 * @property NomenclatureCategory $idParent
 * @property NomenclatureCategory[] $nomenclatureCategories
 */
class NomenclatureCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nomenclature_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id_parent'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['id_parent'], 'exist', 'skipOnError' => true, 'targetClass' => NomenclatureCategory::className(), 'targetAttribute' => ['id_parent' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Категорія',
            'id_parent' => 'Батьківська категорія',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomenclatures()
    {
        return $this->hasMany(Nomenclature::className(), ['id_nomenclature_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdParent()
    {
        return $this->hasOne(NomenclatureCategory::className(), ['id' => 'id_parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomenclatureCategories()
    {
        return $this->hasMany(NomenclatureCategory::className(), ['id_parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildCategory()
    {
        return $this->hasMany(NomenclatureCategory::className(), ['id_parent' => 'id']);
    }
}
