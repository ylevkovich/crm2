<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::$classMap['yii\helpers\BaseHtml'] = '@common/components/BaseHtml.php';
Yii::$classMap['yii\grid\ActionColumn'] = '@common/components/ActionColumn.php';
