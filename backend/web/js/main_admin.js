/**
 * Created by yurii.l on 17.07.17.
 */
$(document).ready(function () {
    fixedBlock();

    //delete users
    // $('#delete-several_btn').on('click', function () {
    //     var ids = $('#w0').yiiGridView('getSelectedRows');
    //     if (ids == '') {
    //         alert('Виберіть спочатку');
    //         return false;
    //     }
    //     $('input[name=ids_to_delete]').val(ids);
    //
    //     $('#delete-several_form').submit();
    // });

    //Send notification
    // $('#send-reminder_btn').on('click', function () {
    //     var ids = $('#w0').yiiGridView('getSelectedRows');
    //
    //     if (ids == '') {
    //         alert('Виберіть спочатку');
    //         return false;
    //     }
    //
    //     $('input[name=ids_to_remind]').val(ids);
    //
    //     $('#send-reminder_form').submit();
    // });

    //create company (if permissive documents)
    $('#company-document_access').on('click', function () {

        var term = $('.company-form .term');
        if ($('#company-document_access:checked').val() == 1) {
            term.show();
        } else {
            term.hide();
        }

    });

    //create user change role
    $('#user-role').on('change', function () {

        var role = $(this).find('input[type=radio]:checked').val();

        $.ajax({
            url: '/admin/company/get-companies-by-role?role=' + role,
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#user-id_company').html(data);
            },
            error: function () {
                console.log('Ajax query error in user-create.js');
            }
        });
    });

    //create company change role
    $('#company-role').on('change', function () {
        //var role = $(this).find('input[type=radio]:checked').val();
        var role = $(this).find('option:selected').val();
        // console.log(role);
        //console.log($(this));
        if (role == 4 || role == 6) {
            $('.buyers').show();
            $('.activity').hide();
        } else {
            $('.buyers').hide();
            $('.activity').show();
        }

        if(role == 6){
            $('.brokers').show();
        } else {
            $('.brokers').hide();
        }

        $.ajax({
            url: '/admin/user/get-users-by-role?role=' + role,
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#company-users').html(data);
            },
            error: function () {
                console.log('Ajax query error in company-create.js');
            }
        });
    });

    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        orientation: "bottom",
        format: "dd/mm/yyyy"
    });


    //click on back button with modal confirm
    $('#back-button').on('click', function () {
        $('#confirm-modal').modal('show');
    });

    //click on delete several button with modal confirm
    $('#delete-several-button').on('click', function () {
        var ids = $('.grid-view').yiiGridView('getSelectedRows');
        if (ids == '') {
            return false;
        }
        $('input[name=ids_to_delete]').val(ids);
        $('#confirm-delete-modal').modal('show');
    });

    //Submit  several delete on modal
    $('#submit-several-delete').on('click', function () {
        $('#delete-several_form').submit();
    });

    //click on reminder several button with modal confirm
    $('#reminder-several-button').on('click', function () {
        var ids = $('.grid-view').yiiGridView('getSelectedRows');
        if (ids == '') {
            return false;
        }
        $('input[name=ids_to_remind]').val(ids);
        $('#confirm-reminder-modal').modal('show');
    });

    //Submit  several reminder on modal
    $('#submit-several-reminder').on('click', function () {
        $('#send-reminder_form').submit();
    });

    // select2
    $('.js-smart-select').select2({
        placeholder: '',
        allowClear: true,
        "language": {
            "noResults": function(){
                return "Результата не найдено";
            }
        }
    });

    //custom header table
    jQuery(function() {
        setTimeout(function () {


        var elemLeng = $('.admin-table thead tr:eq(1) td').length - 1,
            pix = 0;

        $('.admin-table thead tr:eq(1) td').each(function (i) {
            if ( i == elemLeng ){
                pix = 1;
            }
            //$('.custom-table-h-box .table-cell:eq('+i+')').css('width' , $(this).outerWidth( true ) - pix);
            $('.custom-table-h-box .table-cell:eq('+i+')').css('width' , $(this)[0].getBoundingClientRect().width - pix);
        });
            $('.custom-table-h-box').removeClass('hide');
        }, 100);

    });

    //custom table fixed header
    // if ($('.admin-table').length){
    //     $(window).scroll(function() {
    //         $('.custom-table-h-box').css('top',$(window).scrollTop());
    //     });
    // }

    $('#export-several-button').on('click', function () {
        var ids = $('.grid-view').yiiGridView('getSelectedRows');
        if (ids == '') {
            return false;
        }
        $('input[name=ids_to_export]').val(ids);
        $('#export-several_form').submit();

    });


});
function fixedBlock() {

    $('.fixed-box').css('top', $('header').outerHeight());
    $('.main-table-box').css('margin-top', $('header').outerHeight() +  $('.fixed-box').outerHeight());

    if($( window ).width() > 1200){
        $('.custom-table-h-box').css('top',  $('header').outerHeight() +  $('.fixed-box').outerHeight());
    }

}
