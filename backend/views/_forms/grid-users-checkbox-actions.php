<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<?php $form = ActiveForm::begin([
    'method' => 'post',
    'action' => ['user/delete-several'],
    'options' => [
        'class' => 'ajax-submit',
        'id' => 'delete-several_form',
    ],
]); ?>

<?= Html::hiddenInput('ids_to_delete', 'null') ?>

<?php ActiveForm::end(); ?>


<?php $form = ActiveForm::begin([
    'method' => 'post',
    'action' => ['user/send-reminder'],
    'options' => [
        'class' => 'ajax-submit',
        'id' => 'send-reminder_form',
    ],
]); ?>

<?= Html::hiddenInput('ids_to_remind', 'null') ?>

<?php ActiveForm::end(); ?>