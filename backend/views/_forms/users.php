<?php
use common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin(['id' => 'form-signup']); ?>

<?= $form->field($model, 'role')->dropDownList(User::getUsersRolesArray()) ?>

<?= $form->field($model, 'email', [
    'options' => [
        'class' => 'form-group __custom'
    ]]) ?>

<?= $form->field($model, 'full_name', [
    'options' => [
        'class' => 'form-group __custom'
    ]]) ?>

<?= $form->field($model, 'phone', [
    'options' => [
        'class' => 'form-group __custom'
    ]]) ?>

<?= $form->field($model, 'status')->dropDownList(User::STATUS_TRANSLATE) ?>

<?= $form->field($model, 'hidden_auction_access')->checkbox() ?>

<?= $form->field($model, 'legal_person', [
    'options' => [
        'class' => 'form-group __custom'
    ]])->label('Повна назва юридичної особи або підприємця') ?>

<?= $form->field($model, 'idrpo', [
    'options' => [
        'class' => 'form-group __custom'
    ]]) ?>

<?= $form->field($model, 'id_company')->dropDownList($companies,
        [
            'class' => 'form-control js-smart-select',
        ]
    )->label('Вибрати компанію') ?>

    <div class="form-group">
        <?= Html::button('Назад', ['class' => 'button __default', 'id' => "back-button"]) ?>
        <?= Html::submitButton('Зберегти', ['class' => 'button __info']) ?>
    </div>

<?php ActiveForm::end(); ?>



