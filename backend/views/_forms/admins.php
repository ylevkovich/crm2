<?php
use common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin(['id' => 'form-signup']); ?>

<?= $form->field($model, 'status')->dropDownList(User::STATUS_TRANSLATE) ?>
<?= $form->field($model, 'full_name', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]]) ?>
<?= $form->field($model, 'email', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]]) ?>
<?= $form->field($model, 'position', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]]) ?>
<?= $form->field($model, 'legal_person', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]]) ?>
<?= $form->field($model, 'role')->dropDownList(User::getAdminRolesArray()) ?>
<?= $form->field($model, 'new_password', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]])->passwordInput() ?>
    <div class="form-group">
        <?= Html::button('Назад', ['class' => 'button __default', 'id' => "back-button"]) ?>
        <?= Html::submitButton('Зберегти', ['class' => 'button __info']) ?>
    </div>
<?php ActiveForm::end(); ?>