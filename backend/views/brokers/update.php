<?php

use yii\helpers\Html;
$this->title = 'Редагування користувача: ' . $model->full_name;

//Modal config
$this->params['modal_link'] = \yii\helpers\Url::previous();
?>
<div class="main-form-box">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-8">
                <?= $this->render('../_forms/users', ['model' => $model,'companies'=>$companies]) ?>
            </div>
        </div>
    </div>
</div>