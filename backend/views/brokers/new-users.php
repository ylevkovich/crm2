<?php

use yii\helpers\Html;
\yii\helpers\Url::remember();
$this->title = $title;
?>
    <div class="fixed-box">
        <div class="container">
            <h1 ><?= Html::encode($this->title) ?></h1>
            <div class="action-box">
<?=\backend\widgets\SearchWidget::widget(['model'=>$searchModel,'attribute'=>'search'])?>
                <div class="btn-box">
                    <?= Html::a('Додати нового користувача', 'create', ['class' => 'button __info']) ?>
                    <?= Html::button('Відправити повторне сповіщення', ['id' => 'reminder-several-button','class' => 'button __default']) ?>
                    <?= Html::button('Видалити позначене', ['id' => 'delete-several-button','class' => 'button __danger']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main-table-box">
        <div class="container">
            <div class="main-table-box-inner">
                <?= $this->render('../_grids/users', ['dataProvider' => $dataProvider,'searchModel'=>$searchModel]) ?>
            </div>
        </div>
    </div>
