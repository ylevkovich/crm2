<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NomenclatureCategory */
/* @var $categories array */

$this->title = 'Додати номерклатуру';
$this->params['breadcrumbs'][] = ['label' => 'Nomenclature Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<br>
<br>
<br>
<br>
<br>
<div class="nomenclature-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
    ]) ?>

</div>