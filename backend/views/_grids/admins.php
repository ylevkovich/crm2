<?php
use common\models\User;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<?= \backend\components\GridViewCustom::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary' => "{begin} - {end} з {totalCount}",
    'tableOptions' => [
        'class' => 'table  table-bordered text-center admin-table'
    ],
    'columns' => [
        ['class' => CheckboxColumn::className()],
        [
            'attribute' => 'status',
            'value' => 'statusname',
            'filter' => Html::activeDropDownList($searchModel, 'status', User::STATUS_TRANSLATE, ['class' => 'form-control', 'prompt' => 'Не обрано']),
        ],
        'email',
        'full_name',
        'legal_person',
        'position',
        [
            'attribute' => 'role',
            'value' => 'rolename',
            'filter' => Html::activeDropDownList($searchModel, 'role', User::getAdminRolesArray(), ['class' => 'form-control', 'prompt' => 'Не обрано']),

        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ],
]); ?>


<?= $this->render('../_forms/grid-users-checkbox-actions') ?>
