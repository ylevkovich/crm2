<?php

use common\models\Company;
use yii\grid\CheckboxColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="main-table-box">
        <div class="container">
            <div class="main-table-box-inner">
                <?= \backend\components\GridViewCustom::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => "{begin} - {end} з {totalCount}",
                    'tableOptions' => [
                        'class' => 'table  table-bordered text-center admin-table'
                    ],
                    'columns' => [
                        ['class' => CheckboxColumn::className()],

                        'idrpo',
                        'name',
                        'manager_full_name',
                        [
                            'attribute' => 'auction_type',
                            'content' => function ($model){

                                return Company::auctionTypeTranslate()[$model->auction_type] ?? 'не відомо';
                            },
                            'filter' => [Company::AUCTION_TYPE_UNTREATED_WOOD => Company::auctionTypeTranslate()[Company::AUCTION_TYPE_UNTREATED_WOOD]],
                        ],
                        [
                            'attribute' => 'accreditation',
                            'content' => function ($model){
                                return "<div class='circle ".($model->accreditation?'confirmed':'')."'></div>";
                            },
                            'filter' => ['1' => 'Позначено', '0' => 'Не позначено'],
                        ],
                        [
                            'attribute' => 'document_package',
                            'content' => function ($model){
                                return "<div class='circle ".($model->document_package?'confirmed':'')."'></div>";
                            },
                            'filter' => ['1' => 'Позначено', '0' => 'Не позначено'],
                        ],
                        [
                            'attribute' => 'document_access',
                            'content' => function ($model){
                                return "<div class='circle ".($model->document_access?'confirmed':'')."'></div>";
                            },
                            'filter' => ['1' => 'Позначено', '0' => 'Не позначено'],
                        ],
                        [
                            'attribute' => 'localy_reworker',
                            'content' => function ($model){
                                return "<div class='circle ".($model->localy_reworker?'confirmed':'')."'></div>";
                            },
                            'filter' => ['1' => 'Позначено', '0' => 'Не позначено'],
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Дії',
                            'template' => '{update} {delete}',
                        ]
                    ],
                ]); ?>

                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'action' => ['delete-several'],
                    'options' => [
                        'class' => 'ajax-submit',
                        'id' => 'delete-several_form',
                    ],
                ]); ?>

                <?= Html::hiddenInput('ids_to_delete', 'null') ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>