<?php
use common\models\User;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<?= \backend\components\GridViewCustom::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary' => "{begin} - {end} з {totalCount}",
    'tableOptions' => [
        'class' => 'table  table-bordered text-center admin-table'
    ],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn'
        ],
        [
            'class' => CheckboxColumn::className()
        ],
        'id',
        [
            'attribute' => 'status',
            'value' => 'statusname',
            'filter' => Html::activeDropDownList($searchModel, 'status', User::STATUS_TRANSLATE, ['class' => 'form-control', 'prompt' => 'Не обрано']),
        ],
        [
            'attribute' => 'hidden_auction_access',
            'content' => function ($model, $key, $index, $column) {
                return "<div class='circle ".($model->hidden_auction_access?'confirmed':'')."'></div>";
                //return Html::checkbox('hidden_auction_access', $model->hidden_auction_access, ['disabled' => 'disabled']);
            },
            'filter' => Html::activeDropDownList($searchModel, 'hidden_auction_access', ['1' => 'Доступний', '0' => 'Не доступний'], ['class' => 'form-control', 'prompt' => 'Не обрано']),
        ],
        'email',
        'full_name',
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Дії',
            'template' => '{update} {delete}',
        ],
    ],
]); ?>





<?= $this->render('../_forms/grid-users-checkbox-actions') ?>
