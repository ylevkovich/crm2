<?php

use common\models\User;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $title string */

$this->title = $title;
\yii\helpers\Url::remember();
?>

<!--<div class="main-box">-->
<div class="fixed-box">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="action-box">

            <?=\backend\widgets\SearchWidget::widget(['model'=>$searchModel,'attribute'=>'search'])?>
            <div class="btn-box">
                <?= Html::a('Додати нового користувача', '/admin/personnel/create-admin-personnel', ['class' => 'button __info']) ?>
                <?= Html::button('Видалити позначене', ['class' => 'button __danger', 'id' => "delete-several-button"]) ?>
            </div>

        </div>
        <!--            <div class="custom-table-h-box">-->
        <!---->
        <!--            </div>-->
    </div>
</div>
<div class="main-table-box">
    <div class="container">
        <div class="main-table-box-inner">
            <?= $this->render('../_grids/admins', ['dataProvider' => $dataProvider,'searchModel'=>$searchModel]) ?>
        </div>
    </div>
</div>
<!--</div>-->
