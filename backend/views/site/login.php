<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вхід';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-form-box">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>Будь ласка, заповніть наступні поля для входу:</p>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                    <?= $form->field($model, 'email', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]])->textInput(['autofocus' => false]) ?>

                    <?= $form->field($model, 'password', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]])->passwordInput()->label('Пароль') ?>

                    <?= $form->field($model, 'rememberMe')->checkbox()->label('Запам\'ятати мене') ?>

                    <div class="form-group">
                        <?= Html::submitButton('Війти', ['class' => 'button __info', 'name' => 'login-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
