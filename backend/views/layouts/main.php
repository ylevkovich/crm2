<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="fixed-padding">
<?php $this->beginBody() ?>
<header>
    <div class="container">
        <div class="header-inner">
            <a class="header-logo" href="<?= Yii::$app->homeUrl ?>">
                <img src="/admin/images/logo.png" alt="logo">
            </a>
            <?php if (Yii::$app->user->identity) { ?>
                <div class="header-nav">
                    <ul class="header-menu">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0);">Користувачі</a>
                            <ul class="header-menu-inner">
                                <li>
                                    <a href="/admin/personnel">Адміністративний перcонал</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Продавці</a>
                                    <ul class="header-menu-inner-sub">
                                        <li>
                                            <a href="/admin/sellers/new-sellers">Нові користувачі</a>
                                        </li>
                                        <li>
                                            <a href="/admin/sellers/accepted-sellers">Підтвержені користувачі</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Покупці</a>
                                    <ul class="header-menu-inner-sub">
                                        <li>
                                            <a href="/admin/buyers/new-buyers">Нові користувачі</a>
                                        </li>
                                        <li>
                                            <a href="/admin/buyers/accepted-buyers">Підтвержені користувачі</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Брокеры</a>
                                    <ul class="header-menu-inner-sub">
                                        <li>
                                            <a href="/admin/brokers/new-brokers">Нові користувачі</a>
                                        </li>
                                        <li>
                                            <a href="/admin/brokers/accepted-brokers">Підтвержені користувачі</a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </li>
                        <li role="presentation">
                            <a href="javascript:void(0);">Компанії</a>
                            <ul class="header-menu-inner">
                                <li>
                                    <a href="/admin/company/seller">Компанії-продавці</a>
                                </li>
                                <li>
                                    <a href="/admin/company/buyer">Компанії-покупці</a>
                                </li>
                                <li>
                                    <a href="/admin/company/broker">Компанії-брокери</a>
                                </li>
                                <!--                                <li>-->
                                <!--                                    <a href="/admin/company/create">Додати</a>-->
                                <!--                                </li>-->
                            </ul>
                        </li>
                        <li role="presentation">
                            <a href="#">Довідники</a>
                            <ul class="header-menu-inner">
                                <li>
                                    <a href="/admin/nomenclature">Номенклатура</a>
                                </li>
                                <li>
                                    <a href="/admin/company/buyer">Компанії-покупці</a>
                                </li>
                            </ul>
                        </li>
                        <li role="presentation">
                            <a href="/bidding">Торги</a>
                        </li>
                        <li role="presentation">
                            <a href="#">Розсилка</a>
                        </li>
                    </ul>
                    <div class="header-username">
                        <?= !Yii::$app->user->isGuest ? Yii::$app->user->identity->full_name : null ?>
                    </div>
                    <a class="header-logout" href="<?= \yii\helpers\Url::toRoute(['site/logout']) ?>">
                        <img src="/admin/images/svg/logout.svg">
                    </a>
                </div>
                <div class="mob-header-nav" id="mob-menu-open">
                    <div class="mob-menu-lem mob-menu-lem-1"></div>
                    <div class="mob-menu-lem mob-menu-lem-2"></div>
                    <div class="mob-menu-lem mob-menu-lem-3"></div>
                </div>
                <ul class="mob-header-menu">

                </ul>
            <?php } ?>
        </div>
    </div>
</header>


<?= $content ?>
<?= Alert::widget() ?>



<!--CONFIRM MODALS -->

<?php if (isset($this->params['modal_link'])) { ?>
    <div id="confirm-modal" class="fade modal" role="dialog" tabindex="-1">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4>Інформація не була збережена, ви справді бажаєте покинути сторінку?</h4>
                </div>
                <div class="modal-body">
                    <a class="button __info" href="<?= $this->params['modal_link'] ?>">Так</a>
                </div>

            </div>
        </div>
    </div>
<?php } ?>

<div id="confirm-delete-modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Ви дійсно бажаєте видалити позначені записи?</h4>
            </div>
            <div class="modal-body">
                <a class="button __info" id="submit-several-delete">Так</a>
            </div>

        </div>
    </div>
</div>

<div id="confirm-reminder-modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Ви дійсно бажаєте відправити обраним повторне сповіщення?</h4>
            </div>
            <div class="modal-body">
                <a class="button __info" id="submit-several-reminder">Так</a>
            </div>

        </div>
    </div>
</div>
<!--END CONFIRM MODALS -->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
