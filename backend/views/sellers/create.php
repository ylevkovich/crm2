<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\user */
/* @var $companies array of companies with Ids */

$this->title = 'Додати нового користувача';

//Modal config
$this->params['modal_link'] = \yii\helpers\Url::previous();
?>
<div class="main-form-box">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-8">
                <?= $this->render('../_forms/users', ['model' => $model, 'companies' => $companies]) ?>
            </div>
        </div>
    </div>
</div>