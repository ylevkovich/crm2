<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Bidding */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bidding-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Назва торгів') ?>

    <?= $form->field($model, 'is_active')->checkbox()->label('Активен') ?>

    <?= $form->field($model, 'type')->dropDownList([1,2])->label('Тип торгів*') ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true])->label('Місце проведення') ?>

    <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        //'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'time_start')->textInput() ?>

    <?= $form->field($model, 'time_stop')->textInput() ?>

    <?= $form->field($model, 'percent')->textInput() ?>

    <?= $form->field($model, 'summ')->textInput() ?>

    <?= $form->field($model, 'guaranted_percent')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'pdf_reglament')->textInput() ?>

    <?= $form->field($model, 'dogovor')->textInput() ?>

    <?= $form->field($model, 'dogovor_datetime_start')->textInput() ?>

    <?= $form->field($model, 'dogovor_datetime_stop')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
