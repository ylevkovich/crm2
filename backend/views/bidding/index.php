<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Biddings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bidding-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bidding', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'is_active',
            'type',
            'city',
            // 'date',
            // 'time_start',
            // 'time_stop',
            // 'percent',
            // 'summ',
            // 'guaranted_percent',
            // 'status',
            // 'pdf_reglament',
            // 'dogovor',
            // 'dogovor_datetime_start',
            // 'dogovor_datetime_stop',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
