<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'idrpo',
            'auction_type',
            'type',
            'accreditation',
            'document_package',
            'email:email',
            'account_number',
            'bank_name',
            'bank_mfo',
            'individual_tax_number',
            'certificate_of_vat',
            'manager_position',
            'manager_full_name',
            'manager_full_name_genitive_case',
            'zip_code',
            'country',
            'region',
            'area',
            'city',
            'street',
            'phone',
            'basic_contact',
            'role',
            'document_access',
            'localy_reworker',
            'documents_date_from',
            'documents_date_to',
            'documents_issued_by',
            'resident',
            'black_list',
        ],
    ]) ?>

</div>
