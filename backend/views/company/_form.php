<?php

use common\models\Company;
use common\models\User;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */

//Modal config
$this->params['modal_link'] = \yii\helpers\Url::previous();
?>
<div class="row">
    <div class="col-lg-8">
        <div class="company-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?php if($model->isNewRecord) $model->role = Company::ROLE_SELLER ?>
            <?= $form->field($model, 'role')->dropDownList([
                Company::ROLE_SELLER => Company::ROLE_TRANSLATE[Company::ROLE_SELLER],
                Company::ROLE_BUYER => Company::ROLE_TRANSLATE[Company::ROLE_BUYER],
                Company::ROLE_BROKER => Company::ROLE_TRANSLATE[Company::ROLE_BROKER],
            ]) ?>

            <?= $form->field($model, 'email', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'idrpo', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?php if($model->isNewRecord) $model->auction_type = Company::AUCTION_TYPE_UNTREATED_WOOD ?>
            <?= $form->field($model, 'auction_type')->dropDownList([
                Company::AUCTION_TYPE_UNTREATED_WOOD => Company::auctionTypeTranslate()[Company::AUCTION_TYPE_UNTREATED_WOOD],
            ]) ?>

            <div class="activity" style="display:<?= $model->role == Company::ROLE_SELLER ? 'block' : 'none' ?> ">
                <?php if($model->isNewRecord) $model->type = Company::TYPE_FORESTRY ?>

                <?= $form->field($model, 'type')->dropDownList([
                    Company::TYPE_FORESTRY => Company::typeTranslate()[Company::TYPE_FORESTRY],
                    Company::TYPE_AGRO_FORESTRY => Company::typeTranslate()[Company::TYPE_AGRO_FORESTRY],
                    Company::TYPE_MILITARY_FORESTRY => Company::typeTranslate()[Company::TYPE_MILITARY_FORESTRY],
                ]) ?>
            </div>

            <?= $form->field($model, 'account_number', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'bank_name', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'bank_mfo', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'individual_tax_number', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput() ?>

            <?= $form->field($model, 'certificate_of_vat', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput() ?>

            <?= $form->field($model, 'manager_position', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'manager_full_name', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'manager_full_name_genitive_case', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'zip_code', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput() ?>

            <?= $form->field($model, 'country', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'region', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'area', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'city', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'street', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'phone', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'accreditation')->checkbox()->label(false) ?>

            <?= $form->field($model, 'document_package')->checkbox()->label(false) ?>


            <div class="buyers" style="display:<?= $model->role == Company::ROLE_SELLER ? 'none' : 'block' ?> ">
                <hr>

                <?= $form->field($model, 'document_access')->checkbox()->label(false) ?>

                <div class="term" style="display: <?= $model->document_access == 1 ? 'block' : 'none' ?>">

                    <?php !$model->isNewRecord ? $model->documents_date_from = date('d/m/Y', $model->documents_date_from) : null ?>
                    <?= $form->field($model, 'documents_date_from')->textInput(['class' => 'datepicker']) ?>

                    <?php !$model->isNewRecord ? $model->documents_date_to = date('d/m/Y', $model->documents_date_to) : null ?>
                    <?= $form->field($model, 'documents_date_to')->textInput(['class' => 'datepicker']) ?>

                    <?= $form->field($model, 'documents_issued_by')->textInput() ?>

                </div>

                <?= $form->field($model, 'localy_reworker')->checkbox()->label(false) ?>

                <?= $form->field($model, 'resident')->checkbox()->label(false) ?>

                <hr>
            </div>
            <div class="brokers" style="display:<?= $model->role == Company::ROLE_BROKER ? 'block' : 'none' ?> ">
                <?= $form->field($model, 'broker')->checkbox()->label(false) ?>
            </div>


            <?= $form->field($model, 'basic_contact', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'note', [
                'options' => [
                    'class' => 'form-group __custom'
                ]])->textarea(['rows'=>5]) ?>

            <?= $form->field($model, 'company_users[]')->dropDownList( 
                User::getUsersForDropDown($model->role, $model->id),
                [
                    'options' => !$model->isNewRecord ? Company::getSelectedUsersForDropDown($model->id) : [],
                    'class' => 'chosen-select input-md required form-control js-smart-select ',
                    'multiple' => 'multiple',
                ]
            )->label("Вибрати існуючого користувача") ?>

            <div class="form-group">
<!--                --><?php //Modal::begin([
//                    'header' => '<h4>Інформація не була збережена, ви справді бажаєте покинути сторінку?</h4>',
//                    'toggleButton' => [
//                        'label' => 'Назад',
//                        'class' => 'button __default'
//                    ],
//                ]);
//
//                echo Html::a('Так', Url::previous(), ['class' => 'button __info']);
//
//                Modal::end(); ?>

<!--            </div>-->
<!---->
<!--            <div class="form-group">-->
                <?=Html::button('Назад',['class'=>'button __default','id'=>"back-button"])?>
                <?= Html::submitButton('Зберегти', ['class' => $model->isNewRecord ? 'button __info' : 'button __info']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
