<?php

use yii\helpers\Html;

$this->title = $title;
\yii\helpers\Url::remember();
?>


    <div class="fixed-box">
        <div class="container">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="action-box">
<?=\backend\widgets\SearchWidget::widget(['model'=>$searchModel,'attribute'=>'search'])?>
                <div class="btn-box">
                    <?= Html::a('Додати нову компанію', ['create'], ['class' => 'button __info']) ?>
                    <?= Html::button('Видалити позначене', ['id' => 'delete-several-button', 'class' => 'button __danger']) ?>
                </div>
            </div>
        </div>
    </div>



    <?php

    if(Yii::$app->controller->action->id=='seller'){
        echo $this->render('../_grids/company-sellers', ['dataProvider' => $dataProvider,'searchModel'=>$searchModel]);
    }
    if(Yii::$app->controller->action->id=='buyer' ){
        echo $this->render('../_grids/company-buyers', ['dataProvider' => $dataProvider,'searchModel'=>$searchModel]);
    }
    if(Yii::$app->controller->action->id=='broker'){
        echo $this->render('../_grids/company-brokers', ['dataProvider' => $dataProvider,'searchModel'=>$searchModel]);
    }


    ?>
