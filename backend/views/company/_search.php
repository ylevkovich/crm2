<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'idrpo') ?>

    <?= $form->field($model, 'auction_type') ?>

    <?= $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'accreditation') ?>

    <?php // echo $form->field($model, 'document_package') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'account_number') ?>

    <?php // echo $form->field($model, 'bank_name') ?>

    <?php // echo $form->field($model, 'bank_mfo') ?>

    <?php // echo $form->field($model, 'individual_tax_number') ?>

    <?php // echo $form->field($model, 'certificate_of_vat') ?>

    <?php // echo $form->field($model, 'manager_position') ?>

    <?php // echo $form->field($model, 'manager_full_name') ?>

    <?php // echo $form->field($model, 'manager_full_name_genitive_case') ?>

    <?php // echo $form->field($model, 'zip_code') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'region') ?>

    <?php // echo $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'street') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'basic_contact') ?>

    <?php // echo $form->field($model, 'role') ?>

    <?php // echo $form->field($model, 'document_access') ?>

    <?php // echo $form->field($model, 'localy_reworker') ?>

    <?php // echo $form->field($model, 'documents_date_from') ?>

    <?php // echo $form->field($model, 'documents_date_to') ?>

    <?php // echo $form->field($model, 'documents_issued_by') ?>

    <?php // echo $form->field($model, 'resident') ?>

    <?php // echo $form->field($model, 'black_list') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
