<?php
use common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin(['id' => 'form-signup']); ?>

<?= $form->field($model, 'role')->radioList([
    User::ROLE_BUYER => User::ROLE_TRANSLATE[User::ROLE_BUYER],
    User::ROLE_SELLER => User::ROLE_TRANSLATE[User::ROLE_SELLER],
    User::ROLE_BROKER => User::ROLE_TRANSLATE[User::ROLE_BROKER],
])->label('Роль') ?>

<?= $form->field($model, 'email', [
    'options' => [
        'class' => 'form-group __custom'
    ]])->label('Електронна адреса') ?>

<?= $form->field($model, 'full_name', [
    'options' => [
        'class' => 'form-group __custom'
    ]])->textInput()->label('ПІБ') ?>

<?= \frontend\widgets\PhoneWidget::widget([
    'form' => $form,
    'model' => $model
]); ?>

<?= $form->field($model, 'status')->radioList([1 => 'Активний', 2 => 'Не активний'])->label('Статус') ?>

<?= $form->field($model, 'hidden_auction_access')->checkbox(['label' => 'Перегляд прихованих торгів']) ?>

<?= $form->field($model, 'legal_person', [
    'options' => [
        'class' => 'form-group __custom'
    ]])->label('Повна назва юридичної компанії або підприємця') ?>

<?= $form->field($model, 'idrpo', [
    'options' => [
        'class' => 'form-group __custom'
    ]])->label('ЄДРПОУ або ІПН') ?>

<?= $form->field($model, 'id_company')->dropDownList($companies)->label('Вибрати компанію') ?>

    <div class="form-group">
        <?= Html::button('Назад', ['class' => 'button __default', 'id' => "back-button"]) ?>
        <?= Html::submitButton('Зареєструватися', ['class' => 'button __info']) ?>
    </div>

<?php ActiveForm::end(); ?>


