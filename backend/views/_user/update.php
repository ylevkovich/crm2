<?php

use common\models\User;
use frontend\widgets\PhoneWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\user */
/* @var $companies array of companies */

$this->title = 'Редагування користувача: ' . $model->full_name;
//Modal config
$this->params['modal_link'] = \yii\helpers\Url::previous();
?>
<div class="main-form-box">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?php if($model->isAdminPersonnel()):?>
                    <?= $form->field($model, 'role')->radioList([
                        User::ROLE_ADMIN => User::ROLE_TRANSLATE[User::ROLE_ADMIN],
                        User::ROLE_MANAGER => User::ROLE_TRANSLATE[User::ROLE_MANAGER],
                        User::ROLE_ACCOUNTENT => User::ROLE_TRANSLATE[User::ROLE_ACCOUNTENT],
                    ])->label('Роль') ?>
                <?php else: ?>
                    <?= $form->field($model, 'role')->radioList([
                        User::ROLE_BUYER => User::ROLE_TRANSLATE[User::ROLE_BUYER],
                        User::ROLE_SELLER => User::ROLE_TRANSLATE[User::ROLE_SELLER],
                        User::ROLE_BROKER => User::ROLE_TRANSLATE[User::ROLE_BROKER],
                    ])->label('Роль') ?>
                <?php endif; ?>

                <?= $form->field($model, 'email')->label('Електронна адреса') ?>

                <?= $form->field($model, 'new_password')->label('Пароль')->passwordInput() ?>

                <?= $form->field($model, 'full_name')->textInput()->label('ПІБ') ?>

                <?= PhoneWidget::widget([
                    'form' => $form,
                    'model' => $model
                ]); ?>

                <?= $form->field($model, 'status')->radioList([1 => 'Активний', 0 => 'Не активний'])->label('Статус') ?>

                <?= $form->field($model, 'hidden_auction_access')->checkbox(['label' => 'Перегляд прихованих торгів']) ?>

                <?= $form->field($model, 'legal_person')->label('Повна назва юридичної компанії або підприємця') ?>

                <?= $form->field($model, 'idrpo')->label('ЄДРПОУ або ІПН') ?>

                <?= !$model->isAdminPersonnel() ? $form->field($model, 'id_company')->dropDownList($companies)->label('Вибрати компанію') : null?>

                <div class="form-group">
                    <?=Html::button('Назад',['class'=>'button __default','id'=>"back-button"])?>

                    <?= Html::submitButton('Зберегти', ['class' => 'button __info']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>