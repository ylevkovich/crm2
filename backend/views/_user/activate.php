<?php

use yii\helpers\Html;

/* @var $activated */

$this->title = 'Активація';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if($activated): ?>
        <div class="alert alert-success" role="alert">Ваш профіль успішно активовано!</div>
    <?php else: ?>
        <div class="alert alert-danger" role="alert">Ваш профіль не активовано</div>
    <?php endif; ?>

</div>