<?php

use common\models\User;
use frontend\widgets\PhoneWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\user */
/* @var $companies array of companies with Ids*/

$this->title = 'Додати нового користувача';
//Modal config
$this->params['modal_link'] = \yii\helpers\Url::previous();

?>
    <div class="main-form-box">
        <div class="container">
            <h1><?= Html::encode($this->title) ?></h1>

            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'role')->radioList([
                        User::ROLE_BUYER => User::ROLE_TRANSLATE[User::ROLE_BUYER],
                        User::ROLE_SELLER => User::ROLE_TRANSLATE[User::ROLE_SELLER],
                        User::ROLE_BROKER => User::ROLE_TRANSLATE[User::ROLE_BROKER],
                    ])->label('Роль') ?>

                    <?= $form->field($model, 'email', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]] )->label('Електронна адреса') ?>

                    <?= $form->field($model, 'full_name', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]])->textInput()->label('ПІБ') ?>

                    <?= PhoneWidget::widget([
                        'form' => $form,
                        'model' => $model
                    ]); ?>

                    <?= $form->field($model, 'status')->radioList([1 => 'Активний', 2 => 'Не активний'])->label('Статус') ?>

                    <?= $form->field($model, 'hidden_auction_access')->checkbox(['label' => 'Перегляд прихованих торгів']) ?>

                    <?= $form->field($model, 'legal_person', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]])->label('Повна назва юридичної компанії або підприємця') ?>

                    <?= $form->field($model, 'idrpo', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]])->label('ЄДРПОУ або ІПН') ?>

                    <?= $form->field($model, 'id_company')->dropDownList($companies)->label('Вибрати компанію') ?>

                    <div class="form-group">
        <!--                --><?php //Modal::begin([
        //                    'header' => '<h4>Інформація не була збережена, ви справді бажаєте покинути сторінку?</h4>',
        //                    'toggleButton' => [
        //                        'label' => 'Назад',
        //                        'class' => 'button __default'
        //                    ],
        //                ]);
        //
        //                echo Html::a('Так', '/admin/user/new-sellers', ['class' => 'button __info']);
        //
        //                Modal::end(); ?>
                        <?=Html::button('Назад',['class'=>'button __default','id'=>"back-button"])?>
                        <?= Html::submitButton('Зареєструватися', ['class' => 'button __info']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>