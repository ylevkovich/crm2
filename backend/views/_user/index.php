<?php

use yii\grid\CheckboxColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $title string */

$this->title = $title;
//$this->registerJsFile('js/user-index.js', ['depends' => [yii\jui\JuiAsset::className()]]);

?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<!--<div class="main-box">-->
    <div class="fixed-box">
        <div class="container">
            <h1 ><?= Html::encode($this->title) ?></h1>
            <div class="action-box">
                <form class="search">
                    <div class="form-group __custom">
                        <label>ПІБ користувача</label>
                        <input type="text" name="" value="">
                    </div>
                    <button class="button __info">Пошук</button>
                </form>
                <div class="btn-box">
                    <?= Html::button('Відправити повторне сповіщення', ['id' => 'reminder-several-button','class' => 'button __default']) ?>
                    <?= Html::button('Видалити позначене', ['id' => 'delete-several-button','class' => 'button __danger']) ?>
                </div>
            </div>
            <div class="custom-table-h-box">
                <div class="table-cell">
                    <label class="custom-checkbox">
                        <input type="checkbox" class="select-on-check-all" name="selection_all" value="1">
                        <span></span>
                    </label>
                </div>
                <div class="table-cell">#</div>
                <div class="table-cell">Електронна пошта</div>
                <div class="table-cell">ПІБ</div>
                <div class="table-cell">Дії</div>
            </div>
        </div>
    </div>
    <div class="main-table-box">
        <div class="container">
            <div class="main-table-box-inner">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => "{begin} - {end} з {totalCount}",
                    'tableOptions' => [
                        'class' => 'table  table-bordered text-center admin-table'
                    ],
                    'columns' => [
                        [
                            'class' => CheckboxColumn::className(),
                        ],
                        [
                            'class' => 'yii\grid\SerialColumn',
                        ],

                        [
                            'attribute' => 'email',
                            'label' => 'Електронна пошта'
                        ],
                        [
                            'attribute' => 'full_name',
                            'label' => 'ПІБ'
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',

                        ],
                    ],
                ]); ?>

                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'action' => ['delete-several'],
                    'options' => [
                        'class' => 'ajax-submit',
                        'id' => 'delete-several_form',
                    ],
                ]); ?>

                <?= Html::hiddenInput('ids_to_delete', 'null') ?>

                <?php ActiveForm::end(); ?>


                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'action' => ['send-reminder'],
                    'options' => [
                        'class' => 'ajax-submit',
                        'id' => 'send-reminder_form',
                    ],
                ]); ?>

                <?= Html::hiddenInput('ids_to_remind', 'null') ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
<!--</div>-->
