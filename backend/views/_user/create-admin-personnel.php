<?php

use common\models\User;
use frontend\widgets\PhoneWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\user */
/* @var $companies array of companies with Ids*/

$this->title = 'Додати нового користувача';


//Modal config
$this->params['modal_link'] = \yii\helpers\Url::previous();
?>
    <div class="main-form-box">
        <div class="container">

            <h1><?= Html::encode($this->title) ?></h1>

            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'status')->radioList([1 => 'Активний', 2 => 'Не активний'])->label('Статус') ?>

                    <?= $form->field($model, 'full_name', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]])->textInput()->label('ПІБ') ?>

                    <?= $form->field($model, 'email', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]] )->label('Електронна адреса') ?>

                    <?= $form->field($model, 'position', [
                        'options' => [
                            'class' => 'form-group __custom'
                        ]])->textInput()->label('Посада')?>

                    <?= $form->field($model, 'role')->radioList([
                        User::ROLE_ADMIN => User::ROLE_TRANSLATE[User::ROLE_ADMIN],
                        User::ROLE_MANAGER => User::ROLE_TRANSLATE[User::ROLE_MANAGER],
                        User::ROLE_ACCOUNTENT => User::ROLE_TRANSLATE[User::ROLE_ACCOUNTENT],
                    ])->label('Роль') ?>

                    <div class="form-group">
                        <?=Html::button('Назад',['class'=>'button __default','id'=>"back-button"])?>
                        <?= Html::submitButton('Зберегти', ['class' => 'button __info']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>