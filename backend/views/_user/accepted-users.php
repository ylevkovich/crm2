<?php

use yii\grid\CheckboxColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $title string */

$this->title = $title;
//$this->registerJsFile('js/user-index.js', ['depends' => [yii\jui\JuiAsset::className()]]);
?>
<!--<div class="main-box">-->
    <div class="fixed-box">
        <div class="container">
            <h1 ><?= Html::encode($this->title) ?></h1>
            <div class="action-box">
                <form class="search">
                    <div class="form-group __custom">
                        <label>ПІБ користувача</label>
                        <input type="text" name="" value="">
                    </div>
                    <button class="button __info">Пошук</button>
                </form>
                <div class="btn-box">
                    <?= Html::a('Додати нового користувача', 'create', ['class' => 'button __info']) ?>
                    <?= Html::button('Відправити повторне сповіщення', ['id' => 'send-reminder_btn','class' => 'button __default']) ?>
                    <?= Html::button('Видалити позначене', ['id' => 'delete-several-button','class' => 'button __danger']) ?>
                </div>
            </div>
<!--            <div class="custom-table-h-box">-->
<!---->
<!--            </div>-->
        </div>
    </div>
    <div class="main-table-box">
        <div class="container">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => "{begin} - {end} з {totalCount}",
                'tableOptions' => [
                    'class' => 'table  table-bordered text-center admin-table'
                ],
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn'
                    ],
                    [
                        'class' => CheckboxColumn::className()
                    ],
                    [
                        'attribute' => 'status',
                        'label' => 'Статус',
        //                'contentOptions' =>function ($model, $key, $index, $column){
        //                    return ['class' => 'name'];
        //                },
                        'content' => function ($model, $key, $index, $column){
                            return $model->status ? 'Активний' : ' Не активний';
                        },
                        'filter' => ['1' => 'Активний', '2' => 'Не активний'],
                    ],
                    [
                        'attribute' => 'hidden_auction_access',
                        'label' => 'Перегляд прихованих торгів',
                        'content' => function ($model, $key, $index, $column){
                            return "<div class='circle ".($model->hidden_auction_access?'confirmed':'')."'></div>";
                            //return Html::checkbox('hidden_auction_access', $model->hidden_auction_access, ['disabled' => 'disabled']);
                        },
                        'filter' => ['1' => 'Доступний', '0' => 'Не доступний'],
                    ],
                    [
                        'attribute' => 'email',
                        'label' => 'Електронна пошта'
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'ПІБ'
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Дії',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>

        <!--    <div class="countOfRows">-->
        <!--        <ul class="nav nav-pills" role="tablist">-->
        <!--            <li role="presentation" class="active"><a href="#">Кількість записів на даній сторінці <span class="badge">--><?//=$dataProvider->pagination->pageSize?><!--</span></a></li>-->
        <!--            <li role="presentation" class="active"><a href="#">Загальна кількість записів у розділі <span class="badge">--><?//=$dataProvider->totalCount?><!--</span></a></li>-->
        <!--        </ul>-->
        <!--    </div>-->
        <!---->
        <!--    <p>-->
        <!--        --><?//= Html::button('Видалити позначене', ['id' => 'delete-several_btn','class' => 'btn btn-danger']) ?>
        <!---->
        <!--        --><?//= Html::button('Відправити повторне сповіщення усім позначеним користувачам', ['id' => 'send-reminder_btn','class' => 'btn btn-info']) ?>
        <!--    </p>-->

            <?php $form = ActiveForm::begin([
                'method' => 'post',
                'action' => ['delete-several'],
                'options' => [
                    'class' => 'ajax-submit',
                    'id' => 'delete-several_form',
                ],
            ]); ?>

            <?= Html::hiddenInput('ids_to_delete', 'null') ?>

            <?php ActiveForm::end(); ?>


            <?php $form = ActiveForm::begin([
                'method' => 'post',
                'action' => ['send-reminder'],
                'options' => [
                    'class' => 'ajax-submit',
                    'id' => 'send-reminder_form',
                ],
            ]); ?>

            <?= Html::hiddenInput('ids_to_remind', 'null') ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
<!--</div>-->