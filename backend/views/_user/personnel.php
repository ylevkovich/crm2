<?php

use common\models\User;
use yii\bootstrap\Modal;
use yii\grid\CheckboxColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $title string */

$this->title = $title;

?>

<!--<div class="main-box">-->
    <div class="fixed-box">
        <div class="container">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="action-box">
                <form class="search">
                    <div class="form-group __custom">
                        <label>ПІБ користувача</label>
                        <input type="text" name="" value="">
                    </div>
                    <button class="button __info">Пошук</button>
                </form>
                <div class="btn-box">
                    <?= Html::a('Додати нового користувача', '/admin/user/create-admin-personnel', ['class' => 'button __info']) ?>
                    <?=Html::button('Видалити позначене',['class'=>'button __danger','id'=>"reminder-several-button"])?>
                </div>

            </div>
<!--            <div class="custom-table-h-box">-->
<!---->
<!--            </div>-->
        </div>
    </div>
    <div class="main-table-box">
        <div class="container">
            <div class="main-table-box-inner">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => "{begin} - {end} з {totalCount}",
                    'tableOptions' => [
                        'class' => 'table  table-bordered text-center admin-table'
                    ],
                    'columns' => [
                        [
                            'class' => CheckboxColumn::className(),
                        ],

                        [
                            'attribute' => 'status',
                            'content' => function ($model) {
                                return $model->status == 1 ? 'Активний' : 'Не активний';
                            },
                            'filter' => ['1' => 'Активний', '0' => 'Не активний'],
                        ],
                        [
                            'attribute' => 'email',
                            'label' => 'Електронна пошта'
                        ],
                        [
                            'attribute' => 'full_name',
                            'label' => 'ПІБ'
                        ],
                        [
                            'attribute' => 'role',
                            'content' => function ($model) {

                                return User::ROLE_TRANSLATE[$model->role];
                            },
                            'filter' => [
                                    User::ROLE_ADMIN=> User::ROLE_TRANSLATE[User::ROLE_ADMIN],
                                    User::ROLE_MANAGER=> User::ROLE_TRANSLATE[User::ROLE_MANAGER],
                                    User::ROLE_ACCOUNTENT=> User::ROLE_TRANSLATE[User::ROLE_ACCOUNTENT],
                                ],
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',
                        ],
                    ],
                ]); ?>

                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'action' => ['delete-several'],
                    'options' => [
                        'class' => 'ajax-submit',
                        'id' => 'delete-several_form',
                    ],
                ]); ?>

                <?= Html::hiddenInput('ids_to_delete', 'null') ?>

                <?php ActiveForm::end(); ?>


                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'action' => ['send-reminder'],
                    'options' => [
                        'class' => 'ajax-submit',
                        'id' => 'send-reminder_form',
                    ],
                ]); ?>

                <?= Html::hiddenInput('ids_to_remind', 'null') ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
<!--</div>-->
