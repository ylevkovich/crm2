<?php

use common\models\Nomenclature;
use common\models\NomenclatureCategory;
use yii\grid\CheckboxColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\NomenclatureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categories NomenclatureCategory */
/* @var $selected_category integer */
/* @var $childCategoryList array */

$this->title = 'Номенклатура';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nomenclature-index">
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
    <div class="left_block">
        <ul>
            <?php if(!empty($categories)): ?>
                <?php foreach ($categories as $category): ?>

                    <li>
                        <?= $category->name ?>

                        <?php if(!empty($category->childCategory)): ?>
                            <ul>
                                <?php foreach ($category->childCategory as $childCategory): ?>
                                    <li class="<?= $childCategory->id == $selected_category ? 'selected' : ''?>">
                                        <a href="/admin/nomenclature/?id_category=<?= $childCategory->id ?>">
                                            <?=$childCategory->name?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>

                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
    <div class="right_block">
        <h1><?= Html::encode($this->title) ?></h1>

        <br/>
        <br/>
            <?= Html::a('Додати', ['create?id_category=' . $selected_category], ['class' => 'button __info']) ?>
            <?= Html::button('Видалити позначене', ['id' => 'delete-several-button', 'class' => 'button __danger']) ?>
            <?= Html::button('Завантажити документ', ['id' => 'export-several-button', 'class' => 'button __info']) ?>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary' => "{begin} - {end} з {totalCount}",
            'columns' => [
                ['class' => CheckboxColumn::className()],

                'name',
                [
                    'attribute' => 'id_nomenclature_category',
                    'content' => function ($model) use($childCategoryList){
                        return $childCategoryList[$model->id_nomenclature_category];
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'id_nomenclature_category', $childCategoryList, ['class' => 'form-control', 'prompt' => 'Не обрано']),
                ],
                'breed',
                'sort',
                 'diameter',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Дії',
                    'template' => '{update} {delete}',
                ]
            ],
        ]); ?>

        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'action' => ['delete-several'],
            'options' => [
                'class' => 'ajax-submit',
                'id' => 'delete-several_form',
            ],
        ]); ?>
        <?= Html::hiddenInput('ids_to_delete', 'null') ?>
        <?php ActiveForm::end(); ?>


        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'action' => ['export-several'],
            'options' => [
                'class' => 'ajax-submit',
                'id' => 'export-several_form',
            ],
        ]); ?>
        <?= Html::hiddenInput('ids_to_export', 'null') ?>
        <?php ActiveForm::end(); ?>

        <?php Pjax::end(); ?>
    </div>
</div>
