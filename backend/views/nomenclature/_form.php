<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Nomenclature */
/* @var $form yii\widgets\ActiveForm */
/* @var $category \common\models\NomenclatureCategory */
?>

<div class="nomenclature-form">
<br/>
<br/>
<br/>
    <?php

    $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= $form->field($model, 'id_nomenclature_category')->hiddenInput(['value' => $category->id]) ?>
        <?= Html::label($category->name) ?>
    </div>

    <?= $form->field($model, 'breed')->textInput() ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'diameter')->textInput() ?>

    <?= $form->field($model, 'state_standart')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Зберегти' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
