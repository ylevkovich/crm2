<?php

use yii\grid\CheckboxColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $title string */
\yii\helpers\Url::remember();
$this->title = $title;
?>
    <div class="fixed-box">
        <div class="container">
            <h1 ><?= Html::encode($this->title) ?></h1>
            <div class="action-box">
<?=\backend\widgets\SearchWidget::widget(['model'=>$searchModel,'attribute'=>'search'])?>
                <div class="btn-box">
                    <?= Html::button('Відправити повторне сповіщення', ['id' => 'reminder-several-button','class' => 'button __default']) ?>
                    <?= Html::button('Видалити позначене', ['id' => 'delete-several-button','class' => 'button __danger']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main-table-box">
        <div class="container">
            <div class="main-table-box-inner">
                <?= $this->render('../_grids/users', ['dataProvider' => $dataProvider,'searchModel'=>$searchModel]) ?>
            </div>
        </div>
    </div>
