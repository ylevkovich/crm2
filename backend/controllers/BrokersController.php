<?php

namespace backend\controllers;

use backend\models\BrokersSearch;
use common\models\Company;
use common\models\User;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for user model.
 */
class BrokersController extends Controller
{
    public function actionCreate()
    {
        $model = new User();

        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            $model->id_company = Yii::$app->request->post('User')['id_company'] != '0' ? Yii::$app->request->post('User')['id_company'] : null;
            $model->hidden_auction_access = Yii::$app->request->post('User')['hidden_auction_access'];
            $model->created_at = $model->updated_at = date('U');
            $model->activate_code = \Yii::$app->security->generateRandomString(10);
            $model->setPassword(Yii::$app->security->generateRandomString(6));
            $model->generatePasswordResetToken();

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Збереження выконано');
                if ($model->status == User::STATUS_ACTIVATED) {
                    mail($model->email, 'Статус активовано', 'Ваш обліковий запис активовано');
                }

                if ($model->role == User::ROLE_SELLER) {
                    return $this->redirect(['sellers/update','id'=>$model->id]);
                } elseif ($model->role == User::ROLE_BUYER) {
                    return $this->redirect(['buyers/update','id'=>$model->id]);
                } else {
                    return $this->redirect(['brokers/update','id'=>$model->id]);
                }
            }
        }

        $companies = Company::getCompaniesForDropDown(Company::ROLE_SELLER);

        return $this->render('create', [
            'model' => $model,
            'companies' => $companies
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $id_company = Yii::$app->request->post('User')['id_company'] ?? null;
            $model->id_company = $id_company != '0' ? $id_company : null;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Збереження выконано');
                return $this->refresh();
            }
        }

        $companies = Company::getCompaniesForDropDown($model->role);
        $model->password_hash = null;

        return $this->render('update', [
            'model' => $model,
            'companies' => $companies
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Сторінка не знайдена');
        }
    }

    public function actionNewBrokers()
    {
        $conditions = [
            ['role' => User::ROLE_BROKER],
            ['status' => User::STATUS_NOT_ACTIVATED],
            ['id_company' => NULL],
        ];

        $searchModel = new BrokersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $conditions);

        return $this->render('new-users', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Нові користувачі-брокери',
        ]);
    }

    public function actionAcceptedBrokers()
    {
        $conditions = [
            ['role' => User::ROLE_BROKER],
            ['status' => [User::STATUS_ACTIVATED,User::STATUS_BLOKED]],
        ];

        $searchModel = new BrokersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $conditions);

        return $this->render('accepted-users', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Підтверджені користувачі-брокери'
        ]);
    }

}