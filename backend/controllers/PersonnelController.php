<?php

namespace backend\controllers;

use backend\models\PersonnelSearch;
use common\models\Company;
use Yii;
use common\models\User;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for user model.
 */
class PersonnelController extends Controller
{
    public function actionCreateAdminPersonnel()
    {
        $model = new User();

        if(Yii::$app->request->post() && $model->load(Yii::$app->request->post())){
            $model->created_at = $model->updated_at = date('U');
            $model->activate_code = \Yii::$app->security->generateRandomString(10);
            $model->setPassword(Yii::$app->security->generateRandomString(6));
            $model->generatePasswordResetToken();

            if($model->save(false)){
                if($model->status == User::STATUS_ACTIVATED){
                    mail($model->email, 'Статус активовано','Ваш обліковий запис активовано');
                }

                return $this->redirect(['index']);
            }
        }

        return $this->render('create-admin-personnel', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $id_company = Yii::$app->request->post('User')['id_company'] ?? null;
            $model->id_company = $id_company != '0' ? $id_company : null;

            if($model->save(false)){
                Yii::$app->session->setFlash('success', 'Збереження выконано');
                return $this->refresh();
            }
        }

        $companies = Company::getCompaniesForDropDown($model->role);
        $model->password_hash = null;

        return $this->render('update', [
            'model' => $model,
            'companies' => $companies
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIndex()
    {
        $conditions = [
            ['role' => [User::ROLE_ADMIN, User::ROLE_MANAGER, User::ROLE_ACCOUNTENT]]
        ];

        $searchModel = new PersonnelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $conditions);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Адміністративний персонал'
        ]);
    }
}