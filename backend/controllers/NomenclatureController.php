<?php

namespace backend\controllers;

use common\models\NomenclatureCategory;
use moonland\phpexcel\Excel;
use Yii;
use common\models\Nomenclature;
use common\models\NomenclatureSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NomenclatureController implements the CRUD actions for Nomenclature model.
 */
class NomenclatureController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Nomenclature models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(!empty(Yii::$app->request->get('id_category'))){
            $id_category = Yii::$app->request->get('id_category');
        }else{
            $id_category = NomenclatureCategory::findOne(['id_parent' => !null])['id'] ?? null;
        }

        $categories = NomenclatureCategory::find()
            ->joinWith('childCategory cc')
            ->where([NomenclatureCategory::tableName().'.id_parent' => null])
            ->all();

        $searchModel = new NomenclatureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id_category);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => $categories,
            'selected_category' => $id_category,
            'childCategoryList' => Nomenclature::getChildCategoriesList(),
        ]);
    }

    /**
     * Displays a single Nomenclature model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Nomenclature model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Nomenclature();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['/nomenclature']);
        } else {

            if(empty($id_category = Yii::$app->request->get('id_category'))){
                throw new \InvalidArgumentException('Wrong url - category_id missed');
            }

            $category = NomenclatureCategory::findOne(['id' => $id_category]);

            return $this->render('create', [
                'model' => $model,
                'category' => $category
            ]);
        }
    }

    /**
     * Updates an existing Nomenclature model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/nomenclature/?id_category=' . $model->id_nomenclature_category]);
        } else {
            $category = NomenclatureCategory::findOne(['id' => $model->id_nomenclature_category]);

            return $this->render('update', [
                'model' => $model,
                'category' => $category,
            ]);
        }
    }

    /**
     * Deletes an existing Nomenclature model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Nomenclature model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nomenclature the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nomenclature::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteSeveral()
    {
        if ($ids = Yii::$app->request->post('ids_to_delete')) {
            Nomenclature::deleteAll('id IN (' . $ids . ')');
        }

        return $this->redirect('/admin/nomenclature');
    }

    /**
     * Export nomenclature data by IDs
     *
     * @return \yii\web\Response
     */
    public function actionExportSeveral()
    {
        if ($ids = Yii::$app->request->post('ids_to_export')) {
            $childCategoryList = Nomenclature::getChildCategoriesList();

            Excel::export([
                'models' => Nomenclature::find()->where('id IN (' . $ids . ')')->all(),
                'columns' => [
                    'name',
                    [
                        'attribute' => 'id_nomenclature_category',
                        'value' => function ($model) use($childCategoryList){
                            return $childCategoryList[$model->id_nomenclature_category];
                        }
                    ],
                    'breed',
                    'sort',
                    'diameter',
                ],
            ]);
        }

        Yii::$app->session->setFlash('error', 'Не вірно передані параметри');

        return $this->redirect('/admin/nomenclature');
    }
}
