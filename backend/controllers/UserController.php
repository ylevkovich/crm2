<?php

namespace backend\controllers;

use common\helpers\ArrayHelper;
use common\models\Company;
use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for user model.
 */
class UserController extends Controller
{
    /**
     * Deletes all users what ids in a post query
     * @return Response
     */
    public function actionDeleteSeveral()
    {
        if($ids = Yii::$app->request->post('ids_to_delete')){
            User::deleteAll('id IN (' . $ids . ')');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Sends reminder activate profile for users
     */
    public function actionSendReminder()
    {
        //Todo send mails
        if($ids = Yii::$app->request->post('ids_to_remind')){
            $users = User::find()
                ->where('id IN (' . $ids . ')')
                ->all();

            if(!empty($users)){
                foreach ($users as $user) {
                    $message = "Для активації вашого профілю перейдіть за наступним посиланням: 
                    <a href='" . Yii::$app->request->hostInfo . "/user/activate?activate_code=". $user->activate_code . "'> Посилання </a>";
                    mail($user->email,'активація профіля', $message);
                }
            }
        }

        return $this->goBack();
    }

    /**
     * Finds and returns usesrs in dropdown-style
     *
     * @return string
     */
    public function actionGetUsersByRole()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;

        if($role = Yii::$app->request->get('role')){

            $users = User::getUsersForDropDown($role);

            return ArrayHelper::arrayToDropDown($users);
        }

        return '';
    }


}