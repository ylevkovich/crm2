<?php

namespace backend\controllers;

use common\helpers\ArrayHelper;
use common\models\Company;
use backend\models\CompanySearch;
use common\models\User;
use DateTime;
use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionSeller()
    {
        $conditions = [
            ['role' => Company::ROLE_SELLER]
        ];

        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $conditions);

        Url::remember('/admin/company/seller');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Компанії-продавці'
        ]);
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionBuyer()
    {
        $conditions = [
            ['role' => Company::ROLE_BUYER]
        ];

        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $conditions);

        Url::remember('/admin/company/buyer');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Компанії-покупці'
        ]);
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionBroker()
    {
        $conditions = [
            ['role' => Company::ROLE_BROKER]
        ];

        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $conditions);

        Url::remember('/admin/company/broker');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Компанії-брокери'
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->role == Company::ROLE_SELLER) {
                $model->document_access = null;
                $model->documents_date_from = null;
                $model->documents_date_to = null;
                $model->localy_reworker = null;
                $model->resident = null;
                $model->documents_issued_by = null;
            } else {
                if (!empty(Yii::$app->request->post('Company')['documents_date_from'])) {
                    $model->documents_date_from = \DateTime::createFromFormat("d/m/Y", Yii::$app->request->post('Company')['documents_date_from'])->format('U');
                }

                if (!empty(Yii::$app->request->post('Company')['documents_date_to'])) {
                    $model->documents_date_to = \DateTime::createFromFormat("d/m/Y", Yii::$app->request->post('Company')['documents_date_to'])->format('U');
                }
            }

            if ($model->save() && $model->linkUsersToCompany()) {

                if ($model->role == Company::ROLE_SELLER) {
                    return $this->redirect(['seller']);
                } elseif ($model->role == Company::ROLE_BUYER) {
                    return $this->redirect(['buyer']);
                } else {
                    return $this->redirect(['broker']);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return string|Response
     * @throws Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->role == Company::ROLE_SELLER) {
                $model->document_access = null;
                $model->documents_date_from = null;
                $model->documents_date_to = null;
                $model->localy_reworker = null;
                $model->resident = null;
                $model->documents_issued_by = null;
            } else {
                if (!empty(Yii::$app->request->post('Company')['documents_date_from'])) {
                    $model->documents_date_from = \DateTime::createFromFormat("d/m/Y", Yii::$app->request->post('Company')['documents_date_from'])->format('U');
                }

                if (!empty(Yii::$app->request->post('Company')['documents_date_to'])) {
                    $model->documents_date_to = \DateTime::createFromFormat("d/m/Y", Yii::$app->request->post('Company')['documents_date_to'])->format('U');
                }
            }

            if ($model->save()) {
                if (!$model->cleanUsersFromCompanyId()) {
                    throw new Exception('Error deleting id_company from User.');
                }

                if (!$model->linkUsersToCompany()) {
                    throw new Exception('Error linking id_company to User.');
                }
            }


            return $this->refresh();
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds and returns companies in dropdown-style
     *
     * @return string
     */
    public function actionGetCompaniesByRole()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;

        if ($role = Yii::$app->request->get('role')) {

            $companies = Company::getCompaniesForDropDown($role);

            return ArrayHelper::arrayToDropDown($companies);
        }

        return '';
    }

    /**
     * Deletes companies by IDs
     *
     * @return Response
     */
    public function actionDeleteSeveral()
    {
        if ($ids = Yii::$app->request->post('ids_to_delete')) {
            Company::deleteAll('id IN (' . $ids . ')');
        }

        return $this->redirect(Url::previous());
    }
}
