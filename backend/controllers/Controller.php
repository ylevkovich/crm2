<?php
namespace backend\controllers;

use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller as mainController;

/**
 * Site controller
 */
class Controller extends mainController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->identity) {
            $role = Yii::$app->user->identity->role;
            if ($role != User::ROLE_ADMIN && $role != User::ROLE_MANAGER && $role != User::ROLE_ACCOUNTENT) {
                Yii::$app->user->logout();
            }
        }
        return parent::beforeAction($action);

    }


}
