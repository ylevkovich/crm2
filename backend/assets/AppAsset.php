<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-datepicker3.css',
        'css/select2.min.css',
        'css/site.css',
        'css/font.css',
        'css/media.css',
    ];
    public $js = [
        'js/bootstrap-datepicker.min.js',
        'js/select2.min.js',
        'js/form.js',
        'js/main_admin.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
