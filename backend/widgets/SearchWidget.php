<?php
namespace backend\widgets;
use \yii\widgets\ActiveForm;
use Yii;

class SearchWidget extends \yii\bootstrap\Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $model;
    public $attribute;
    /**
     * @var array the options for rendering the close button tag.
     */


    public function init()
    {
        parent::init();
        if($this->model && $this->attribute){
                $form = ActiveForm::begin(['method'=>'get','options'=>['class'=>'search']]);
                echo $form->field($this->model,$this->attribute,['options'=>['class'=>'form-group __custom']]);
                echo '<button class="button __info">Пошук</button>';
                ActiveForm::end();
        }


    }
}
