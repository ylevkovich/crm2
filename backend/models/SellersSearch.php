<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * userSearch represents the model behind the search form about `common\models\user`.
 */
class SellersSearch extends User
{
    public $search;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_company', 'status', 'role', 'hidden_auction_access', 'created_at', 'updated_at'], 'integer'],
            [['full_name', 'email', 'phone', 'legal_person', 'auth_key', 'password_hash', 'password_reset_token','search'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param $params
     * @param array $conditions
     * @return ActiveDataProvider
     */
    public function search($params, $conditions = [])
    {
        $query = User::find();

        if($conditions){
            $query->where('1=1');

            foreach ($conditions as $condition){
                $query->andWhere($condition);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->pagination->pageSize = 50;
        $dataProvider->sort->defaultOrder = ['full_name'=>SORT_ASC];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_company' => $this->id_company,
            'status' => $this->status,
            'role' => $this->role,
            'idrpo' => $this->idrpo,
            'hidden_auction_access' => $this->hidden_auction_access,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'legal_person', $this->legal_person])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['or',['like', 'email', $this->search],['like', 'full_name', $this->search]])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token]);

        return $dataProvider;
    }
}