<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Company;

/**
 * CompanySearch represents the model behind the search form about `common\models\Company`.
 */
class CompanySearch extends Company
{
    public $search;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'auction_type', 'type', 'accreditation', 'document_package', 'individual_tax_number', 'certificate_of_vat', 'zip_code', 'role', 'document_access', 'localy_reworker', 'documents_date_from', 'documents_date_to', 'documents_issued_by', 'resident', 'black_list'], 'integer'],
            [['name', 'idrpo', 'email', 'account_number', 'bank_name', 'bank_mfo', 'manager_position', 'manager_full_name', 'manager_full_name_genitive_case', 'country', 'region', 'area', 'city', 'street', 'phone', 'basic_contact','search'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $conditions = [])
    {
        $query = Company::find();

        if($conditions){
            $query->where('1=1');

            foreach ($conditions as $condition){
                $query->andWhere($condition);
            }
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->pagination->pageSize = 50;
        $dataProvider->sort->defaultOrder = ['name'=>SORT_ASC];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'auction_type' => $this->auction_type,
            'type' => $this->type,
            'accreditation' => $this->accreditation,
            'document_package' => $this->document_package,
            'individual_tax_number' => $this->individual_tax_number,
            'certificate_of_vat' => $this->certificate_of_vat,
            'zip_code' => $this->zip_code,
            'role' => $this->role,
            'document_access' => $this->document_access,
            'localy_reworker' => $this->localy_reworker,
            'documents_date_from' => $this->documents_date_from,
            'documents_date_to' => $this->documents_date_to,
            'documents_issued_by' => $this->documents_issued_by,
            'resident' => $this->resident,
            'black_list' => $this->black_list,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['or',['like', 'name', $this->search],['like', 'email', $this->search]])
            ->andFilterWhere(['like', 'auction_type', $this->auction_type])
            ->andFilterWhere(['like', 'idrpo', $this->idrpo])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'account_number', $this->account_number])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'bank_mfo', $this->bank_mfo])
            ->andFilterWhere(['like', 'manager_position', $this->manager_position])
            ->andFilterWhere(['like', 'manager_full_name', $this->manager_full_name])
            ->andFilterWhere(['like', 'manager_full_name_genitive_case', $this->manager_full_name_genitive_case])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'basic_contact', $this->basic_contact]);

        return $dataProvider;
    }
}
