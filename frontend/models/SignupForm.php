<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $role;
    public $email;
    public $password;
    public $password_repeat;
    public $full_name;
    public $phone;
    public $legal_person;
    public $idrpo;

    public $confirm;
    public $captcha;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['role', 'in', 'range' => [User::ROLE_BUYER, User::ROLE_SELLER]],

            ['email', 'trim'],
            ['email', 'email', 'message' => 'Електронна адреса не правильного формату'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Дана електронна адреса вже використовується'],

            ['password', 'string', 'length' => [6,255], 'message' => 'Пароль має складатися не манше як з 6 символів'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=> 'Паролі відрізняються' ],

            ['full_name', 'string', 'max' => 255],

            ['captcha', 'captcha', 'message' => 'Капча введена не вірно'],

            ['confirm', 'in', 'range' => [1], 'message' => 'Підтвердіть'],

            ['idrpo', 'string', 'min' => 8, 'message' => 'Дане поле має складатися з 8-10 символів'],
            ['idrpo', 'string', 'max' => 10, 'message' => 'Дане поле має складатися з 8-10 символів'],

            //Todo after fronted phone
            /*[['phone'], PhoneInputValidator::className()],


             * [['phone_number'], 'match', 'pattern' => '/((\+[0-9]{6})|0)[-]?[0-9]{7}/']
             *
             * // All phones will be controlled according to Turkey and formatted to TR Phone Number
            [['phone'], 'udokmeci\yii2PhoneValidator\PhoneValidator','country'=>'TR'],//

            //All phones will be controlled according to value of $model->country_code
            [['phone'], 'udokmeci\yii2PhoneValidator\PhoneValidator','countryAttribute'=>'country_code'],

            //All phones will be controlled according to value of $model->country_code
            //If model has not a country attribute then phone will not be validated
            //If phone is a valid one will be formatted for International Format. default behavior.
            [['phone'], 'udokmeci\yii2PhoneValidator\PhoneValidator','countryAttribute'=>'country_code','strict'=>false,'format'=>true],*/

            [['email', 'role', 'password', 'password_repeat', 'full_name', 'legal_person', 'idrpo', 'confirm', 'captcha', 'phone'], 'required', 'message' => 'Дане поле не може бути пустим'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->id_company = NULL;
        $user->full_name = $this->full_name;
        $user->email = $this->email;
        $user->status = User::STATUS_NOT_ACTIVATED;
        $user->role = $this->role;
        $user->phone = $this->phone;
        $user->idrpo = $this->idrpo;
        $user->hidden_auction_access = User::HIDDEN_AUCTION_DENIED;
        $user->legal_person = $this->legal_person;
        $user->password_reset_token = $user->generatePasswordResetToken();
        $user->created_at = $user->updated_at = date('U');
        $user->activate_code = \Yii::$app->security->generateRandomString(10);

        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }
}
