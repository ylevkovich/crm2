$(document).ready(function(){
    $('body').on('focus', '.form-group.__custom input, .form-group.__custom textarea', function(){
        $(this).closest('.form-group.__custom').addClass('__no-empty');
    });

    $('body').on('blur', '.form-group.__custom input, .form-group.__custom textarea', function(){
        if($(this).val() !== '') return true;

        $(this).closest('.form-group').removeClass('__no-empty');
    });

    $('body').on('change', '.form-group.__custom select', function(){
        if ($(this).closest('.__cabinet-filter').length > 0) return true;

        $(this).closest('.form-group').addClass('__no-empty');
    });

    $('input, textarea, select').not( "input[type='radio']" ).each(function(){
        if($(this).val() === '') return true;

        $(this).closest('.form-group.__custom').addClass('__no-empty');
    });

    $('body').on('click', '.form-group.__custom label', function(){
        var form = $(this).closest('.form-group')
        $(form).find('input').focus();
        $(form).find('textarea').focus();
        $(form).find('select').focus();
    });

});