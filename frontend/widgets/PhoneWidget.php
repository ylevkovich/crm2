<?php
/**
 * Created by PhpStorm.
 * User: Yurii.l
 * Date: 14.07.17
 * Time: 12:38
 */

namespace frontend\widgets;

use yii\base\Widget;

class PhoneWidget extends Widget
{
    public $form;
    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render(
            'phone', [
                'form' => $this->form,
                'model' => $this->model
            ]
        );
    }
}