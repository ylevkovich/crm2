<?php
/**
 * Created by PhpStorm.
 * User: Yurii.l
 * Date: 14.07.17
 * Time: 12:42
 */

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->registerJsFile('js/phoneWidget/counties.js', ['depends' => [yii\jui\JuiAsset::className()]]);
$this->registerJsFile('js/phoneWidget/phonecode.js', ['depends' => [yii\jui\JuiAsset::className()]]);
$this->registerJs("
    $('#phone').phonecode({
        preferCo: 'ua'
    })"
);

$this->registerCssFile('/css/phoneWidget.css');

echo $form->field($model, 'phone')->textInput(['id' => 'phone'])->label('Телефон');