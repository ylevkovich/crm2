<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
//        ['label' => 'Home', 'url' => ['/site/index']],
//        ['label' => 'About', 'url' => ['/site/about']],
//        ['label' => 'Contact', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Реєстрація', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Вхід    ', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->full_name . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
        ?>

    <?php
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <!--    --><?php //if (!Yii::$app->user->isGuest): ?>
    <header style="margin-top: 51px;">
        <div class="container">
            <div class="header-inner">
                <a class="header-logo" href="/">
                    <img src="/images/logo.png" alt="logo">
                </a>
                <div class="header-nav">
                    <ul class="header-menu">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0);">Користувачі</a>
                            <ul class="header-menu-inner">
                                <li>
                                    <a href="">Адміністративний перcонал</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Продавці</a>
                                    <ul class="header-menu-inner-sub">
                                        <li>
                                            <a href="/admin/user/new-sellers">Нові користувачі</a>
                                        </li>
                                        <li>
                                            <a href="/admin/user/accepted-sellers">Підтвержені користувачі</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Покупці</a>
                                    <ul class="header-menu-inner-sub">
                                        <li>
                                            <a href="/admin/user/new-buyers">Нові користувачі</a>
                                        </li>
                                        <li>
                                            <a href="/admin/user/accepted-buyers">Підтвержені користувачі</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">Брокеры</a>
                                </li>

                            </ul>
                        </li>
                        <li role="presentation">
                            <a href="javascript:void(0);">Компанії</a>
                            <ul class="header-menu-inner">
                                <li>
                                    <a href="/admin/company/seller">Компанії-продавці</a>
                                </li>
                                <li>
                                    <a href="/admin/company/buyer">Компанії-покупці</a>
                                </li>
                                <li>
                                    <a href="/admin/company/broker">Компанії-брокери</a>
                                </li>
                                <!--                                <li>-->
                                <!--                                    <a href="/admin/company/create">Додати</a>-->
                                <!--                                </li>-->
                            </ul>
                        </li>
                        <li role="presentation">
                            <a href="#">Довідники</a>
                        </li>
                        <li role="presentation">
                            <a href="#">Торги</a>
                        </li>
                        <li role="presentation">
                            <a href="#">Розсилка</a>
                        </li>
                    </ul>
                    <div class="header-username">
                        Иван
                    </div>
                </div>
            </div>
        </div>
    </header>

<!--    <a href="/admin/user/new-sellers">Нові користувачі-продавці</a><br/>-->
<!--    <a href="/admin/user/accepted-sellers">Підтверджені користувачі-продавці</a><br/>-->
<!--    <a href="/admin/user/create">Додати нового користувача-продавця</a>-->

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
