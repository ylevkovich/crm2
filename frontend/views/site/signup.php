<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use common\models\User;
use frontend\widgets\PhoneWidget;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Сторінка реєстрації нового користувача';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-box">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'role')->radioList([
                        User::ROLE_BUYER => User::ROLE_TRANSLATE[User::ROLE_BUYER],
                        User::ROLE_SELLER => User::ROLE_TRANSLATE[User::ROLE_SELLER]
                    ], ['autofocus' => true])->label('Роль') ?>

                <?= $form->field($model, 'email', [
                    'options' => [
                        'class' => 'form-group __custom'
                    ]] )->label('Електронна адреса') ?>

                <?= $form->field($model, 'password', [
                    'options' => [
                        'class' => 'form-group __custom'
                    ]] )->passwordInput()->label('Пароль') ?>
                <?= $form->field($model, 'password_repeat', [
                    'options' => [
                        'class' => 'form-group __custom'
                    ]] )->passwordInput()->label('Повторно введіть пароль') ?>

                <?= $form->field($model, 'full_name', [
                    'options' => [
                        'class' => 'form-group __custom'
                    ]] )->textInput()->label('ПІБ') ?>

                <?= PhoneWidget::widget([
                        'form' => $form,
                        'model' => $model
                ]); ?>

                <?= $form->field($model, 'legal_person', [
                    'options' => [
                        'class' => 'form-group __custom'
                    ]] )->label('Повна назва юридичної компанії або підприємця') ?>

                <?= $form->field($model, 'idrpo', [
                    'options' => [
                        'class' => 'form-group __custom'
                    ]] )->label('ЄДРПОУ або ІПН') ?>

                <?= $form->field($model, 'confirm')->checkbox()->label('Я підтверджую достовірність наданої мною інформації та даю згоду на збереження та обробку моїх персональних даних') ?>

                <div class="form-group">
                    <?= Captcha::widget([
                        'model' => $model,
                        'attribute' => 'captcha',
                    ]) ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Зареєструватися', ['class' => 'button __info', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
