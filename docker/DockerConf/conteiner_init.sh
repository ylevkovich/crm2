#!/bin/bash -e

	echo "MySQL Admin User: "
	read -e mysqluser
	echo "MySQL Admin Password: "
	read -s mysqlpass
	echo "MySQL Host (Enter for default 'localhost'): "
	read -e mysqlhost
		mysqlhost=${mysqlhost:-localhost}

    echo "Database Name: "
    read -e dbname

    dbuser=${mysqluser}
    dbpass=${mysqlpass}

    export DEBIAN_FRONTEND="noninteractive"
    debconf-set-selections <<< "mysql-server mysql-server/root_password password $mysqlpass"
    debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $mysqlpass"
    apt-get -y install mysql-server
    service mysql start

		echo "============================================"
		echo "Setting up the database."
		echo "============================================"
		#login to MySQL, add database, add user and grant permissions
		dbsetupuser=""
		if [ $dbuser != "root" ]; then
		dbsetupuser="CREATE USER '$dbuser'@'$mysqlhost' IDENTIFIED BY '$dbpass';"
		fi
		dbsetup=" create database $dbname; GRANT ALL PRIVILEGES ON $dbname.* TO $dbuser@$mysqlhost IDENTIFIED BY '$dbpass';FLUSH PRIVILEGES;"
		mysql -u root -p$mysqlpass -e "$dbsetupuser$dbsetup"
		if [ $? != "0" ]; then
			echo "============================================"
			echo "[Error]: Database creation failed. Aborting."
			echo "============================================"
		fi
    service apache2 start
    /bin/bash


