#!/usr/bin/env bash

RUN a2enmod rewrite
service apache2 restart
service mysql restart
mysql -u root -proot -e "Create database asat; GRANT ALL PRIVILEGES ON asat.* TO root@localhost IDENTIFIED BY 'root';FLUSH PRIVILEGES;"
apt-get -y install phpmyadmin
/bin/bash