Installation
===============================

1) git clone git@bitbucket.org:pro_progers/drova.git

2) composer global require "fxp/composer-asset-plugin:^1.3.1"

3) composer install

4) php init

5) php yii migrate/up

6) php -S localhost:8000 -t frontend/web/

7) register user and set `status` = 1 and `role` = 1

8) php -S localhost:8000 -t backend/web/