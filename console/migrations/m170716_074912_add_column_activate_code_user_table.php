<?php

use yii\db\Migration;

class m170716_074912_add_column_activate_code_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'activate_code', $this->string());
    }

    public function down()
    {
        return $this->dropColumn('user', 'activate_code');
    }
}
