<?php

use yii\db\Migration;

class m170803_151516_create_table_bidding extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bidding}}', [
            'id' => $this->string(2)->notNull()->append('PRIMARY KEY'),
            'name' => $this->string(255)->notNull(),
            'is_active' => $this->integer(1)->notNull()->defaultValue('0'),
            'type' => $this->integer(11)->notNull()->defaultValue('0'),
            'city' => $this->string(255)->notNull()->defaultValue('0'),
            'date' => $this->dateTime()->notNull(),
            'time_start' => $this->dateTime()->notNull(),
            'time_stop' => $this->dateTime()->notNull(),
            'percent' => $this->integer(11)->notNull()->defaultValue('0'),
            'summ' => $this->integer(11)->notNull()->defaultValue('0'),
            'guaranted_percent' => $this->integer(11)->notNull()->defaultValue('0'),
            'status' => $this->integer(11)->notNull()->defaultValue('0'),
            'pdf_reglament' => $this->integer(11)->notNull()->defaultValue('0'),
            'dogovor' => $this->integer(11)->notNull()->defaultValue('0'),
            'dogovor_datetime_start' => $this->dateTime()->notNull(),
            'dogovor_datetime_stop' => $this->dateTime()->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%bidding}}');
    }
}
