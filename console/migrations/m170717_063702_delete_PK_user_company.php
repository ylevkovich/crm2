<?php

use yii\db\Migration;

class m170717_063702_delete_PK_user_company extends Migration
{
    public function up()
    {
        $this->dropForeignKey('PK_user_company', 'user');
    }

    public function down()
    {
        echo "m170717_063702_delete_PK_user_company cannot be reverted.\n";

        return false;
    }

}
