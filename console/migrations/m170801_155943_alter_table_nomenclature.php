<?php

use yii\db\Migration;

class m170801_155943_alter_table_nomenclature extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(\common\models\Nomenclature::tableName(), 'breed', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn(\common\models\Nomenclature::tableName(), 'breed', $this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170801_155943_alter_table_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
