<?php

use yii\db\Migration;

class m170724_180837_alter_user extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\User::tableName(), 'position', $this->string());
    }

    public function down()
    {
        $this->dropColumn(\common\models\User::tableName(), 'position');
    }
}
