<?php

use common\models\Company;
use yii\db\Migration;

class m170722_082722_alter_company_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(Company::tableName(), 'account_numger', $this->integer());
        $this->renameColumn(Company::tableName(), 'account_numger', 'account_number' );
        $this->alterColumn(Company::tableName(), 'individual_tax_number', $this->string());
        $this->alterColumn(Company::tableName(), 'note', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn(Company::tableName(), 'account_number', $this->string());
        $this->renameColumn(Company::tableName(), 'account_number', 'account_numger' );
        $this->alterColumn(Company::tableName(), 'individual_tax_number', $this->integer());
        $this->alterColumn(Company::tableName(), 'note', $this->string());
    }
}
