<?php

use yii\db\Migration;

class m170728_130532_alter_company extends Migration
{

    public function up()
    {
        $this->addColumn(\common\models\Company::tableName(), 'broker', $this->integer(1));
    }

    public function down()
    {
        return $this->dropColumn(\common\models\Company::tableName(), 'broker');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170728_130532_alter_company cannot be reverted.\n";

        return false;
    }
    */
}
