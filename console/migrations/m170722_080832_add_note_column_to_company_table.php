<?php

use common\models\Company;
use yii\db\Migration;

/**
 * Handles adding note to table `company`.
 */
class m170722_080832_add_note_column_to_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Company::tableName(), 'note', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Company::tableName(), 'note');
    }
}
