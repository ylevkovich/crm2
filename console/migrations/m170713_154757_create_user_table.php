<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170713_154757_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'id_company' => $this->integer(),
            'full_name' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->boolean()->notNull()->defaultValue(false),
            'role' => $this->integer(2),
            'phone' => $this->string(32),
            'idrpo' => $this->integer(),
            'hidden_auction_access' => $this->boolean(),
            'legal_person' => $this->string(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
        ],$tableOptions);

        $this->addForeignKey('PK_user_company', 'user', 'id_company', 'company', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
        $this->dropForeignKey('PK_user_company', 'user');
    }
}
