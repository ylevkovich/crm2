<?php

use yii\db\Migration;

class m170723_083040_alter_company extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(\common\models\Company::tableName(), 'documents_issued_by', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn(\common\models\Company::tableName(), 'documents_issued_by', $this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170723_083040_alter_company cannot be reverted.\n";

        return false;
    }
    */
}
