<?php

use yii\db\Migration;

class m170728_153150_change_lenght_company_name extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(\common\models\Company::tableName(), 'name', $this->string(255));
    }

    public function safeDown()
    {
        echo "m170728_153150_change_lenght_company_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170728_153150_change_lenght_company_name cannot be reverted.\n";

        return false;
    }
    */
}
