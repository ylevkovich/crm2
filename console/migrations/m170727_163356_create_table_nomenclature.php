<?php

use yii\db\Migration;

class m170727_163356_create_table_nomenclature extends Migration
{
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%nomenclature_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'id_parent' => $this->integer(),

        ],$tableOptions);

        $this->addForeignKey('FK_nomenclature_category_nomenclature_category', 'nomenclature_category', 'id_parent', 'nomenclature_category', 'id');

        $this->createTable('{{%nomenclature}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'id_nomenclature_category' => $this->integer()->notNull(),
            'breed' => $this->integer()->notNull(),
            'sort' => $this->integer(),
            'diameter' => $this->integer()->notNull(),
            'state_standart' => $this->string(),
            'note' => $this->text(),

        ],$tableOptions);

        $this->addForeignKey('FK_nomenclature_nomenclature_category', 'nomenclature', 'id_nomenclature_category', 'nomenclature_category', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('nomenclature_category');
        $this->dropTable('nomenclature');
    }
}

