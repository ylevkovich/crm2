<?php

use yii\db\Migration;

class m170727_094901_alter_user extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(\common\models\User::tableName(), 'idrpo', $this->string(20));
    }

    public function safeDown()
    {
       $this->alterColumn(\common\models\User::tableName(), 'idrpo', $this->string(20));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170727_094901_alter_user cannot be reverted.\n";

        return false;
    }
    */
}
